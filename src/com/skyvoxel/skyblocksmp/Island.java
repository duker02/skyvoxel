package com.skyvoxel.skyblocksmp;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class Island {
	public int x;
	public int z;
	public String owner;
	public boolean visit = true;
	public boolean customSpawn = false;
	public Location spawn = null;
	public double x9, y9, z9;
	public float yaw;
	public float pitch;
	public World world;
	public String worldname = "SkyBlock";

	public Island(int xx, int zz) {
		x = xx;
		z = zz;
	}

	public int getCenterX() {
		int cx = x + (Config.WIDTH / 2);
		return cx;
	}

	public int getCenterY() {
		return Config.HEIGHT;
	}

	public int getCenterZ() {
		int cz = z + (Config.LENGTH / 2);
		return cz;
	}

	public Location getSpawn() {
		Config.load();
		if(spawn != null){
			return spawn;
		}else{
			return getDefaultSpawn();
		}
	}

	public Location getDefaultSpawn() {
		world = SkyBlockManager.getInstance().getEmptyWorld();
		Location loc1 = new Location(world, getCenterX(), getCenterY() + 3,
				getCenterZ(), 0f, 0f);
		while (!loc1.getBlock().isEmpty()
				&& !loc1.getBlock().getRelative(0, 1, 0).isEmpty()) {
			loc1.add(0, 1, 0);
			if (loc1.getY() >= world.getMaxHeight())
				break;
		}
		return loc1;
	}

	public void setCustomSpawn(Player p) {
		customSpawn = true;
		world = SkyBlockManager.getInstance().getEmptyWorld();
		x9 = p.getLocation().getX();
		y9 = p.getLocation().getY();
		z9 = p.getLocation().getZ();
		yaw = p.getLocation().getYaw();
		pitch = p.getLocation().getPitch();
		spawn = new Location(world, (double) x9, (double) y9, (double) z9, yaw,
				pitch);
		Config.save();
	}

	public void setDefaultSpawn() {
		customSpawn = false;
		spawn = null;
		Config.save();
	}

	public int getID() {
		return x * z;
	}
}