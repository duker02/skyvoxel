package com.skyvoxel.skyblocksmp.commands;

import static org.bukkit.ChatColor.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.server.v1_7_R1.EntityPlayer;
import net.minecraft.server.v1_7_R1.PacketPlayOutSpawnEntityLiving;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.potion.Potion;

import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.skyvoxel.skyblocksmp.Config;
import com.skyvoxel.skyblocksmp.Island;
import com.skyvoxel.skyblocksmp.Party;
import com.skyvoxel.skyblocksmp.SkyBlockManager;
import com.skyvoxel.skyblocksmp.SkyBlockSMP;

public class SkyBlockCommand extends BaseCommand {
	public static HashMap<String, String> invites = new HashMap<String, String>();
	public static ArrayList<Player> confirm = new ArrayList<Player>();
	public static ArrayList<String> l2 = new ArrayList<String>();
	public static ArrayList<String> l3 = new ArrayList<String>();
	private static boolean on = false;
	private static boolean cont = false;

	Potion potion;

	public SkyBlockCommand(SkyBlockSMP plugin) {
		super(plugin);
	}

	private static SkyBlockCommand instance = new SkyBlockCommand(
			SkyBlockSMP.getInstance());

	public static SkyBlockCommand getInstance() {
		return instance;
	}

	@Override
	public void onPlayerCommand(Player p, String[] cmd) {
		SkyBlockManager sbm = SkyBlockManager.getInstance();
		if (cmd.length == 1) {
			if (!sbm.hasIsland(p))
				sbm.createNewIsland(p);
			/**
			 * p.sendMessage(GRAY+"===>"+DARK_GREEN+" SkyVoxel "+GRAY+"<===");
			 * if(p.hasPermission("skyblock.island") && sbm.hasIsland(p))
			 * p.sendMessage
			 * (GRAY+"/skyblock tp "+WHITE+"- Teleport to your island");
			 * if(p.hasPermission("skyblock.island") && sbm.hasIsland(p))
			 * p.sendMessage
			 * (GRAY+"/skyblock new "+WHITE+"- Reset your island.");
			 * if(p.hasPermission("skyblock.island") && !sbm.hasIsland(p))
			 * p.sendMessage
			 * (GRAY+"/skyblock new "+WHITE+"- Create a new island.");
			 * if(p.hasPermission("skyblock.island"))
			 * p.sendMessage(GRAY+"/skyblock list "
			 * +WHITE+"- List the SkyBlock challenges");
			 * if(p.hasPermission("skyblock.party") && sbm.hasIsland(p))
			 * p.sendMessage(GRAY+"/skyblock invite <player> "+WHITE+
			 * "- Invite someone to your island");
			 * if(invites.containsValue(p.getName()) &&
			 * p.hasPermission("skyblock.party")){
			 * p.sendMessage(GRAY+"/skyblock accept "
			 * +WHITE+"- Accept the invite");
			 * p.sendMessage(GRAY+"/skyblock decline "
			 * +WHITE+"- Decline the invite"); }
			 **/
			if (sbm.hasParty(p)) {
				Party pa = sbm.getParty(p);
				if (pa.members.size() == 0 && pa.moderators.size() == 0) {
					p.sendMessage(RED
							+ "Your party has no members. Clearing party from server config...");
					Config.PARTIES.remove(pa);
					Config.save();
					p.sendMessage(GRAY + "Party removed successfully :)");
				}
			}
			sbm.openSkyGUI(p);
			if (sbm.hasParty(p) && p.hasPermission("skyblock.party")) {
				/**
				 * p.sendMessage(GRAY+"/skyblock leave "+WHITE+
				 * "- Leave your party");
				 * p.sendMessage(GRAY+"/skyblock kick <player> "
				 * +WHITE+"- Kick someone from your party");
				 * p.sendMessage(GRAY+"/skyblock makeleader <player> "
				 * +WHITE+"- Make someone else leader");
				 * p.sendMessage(GRAY+"/skyblock makemod <player> " +WHITE+
				 * "- Make somebody a moderator (same commands as the leader)");
				 * p.sendMessage(GRAY+"/skyblock demote <player>"
				 * +WHITE+" - Demote somebody from being a moderator.");
				 **/
				Party party = sbm.getParty(p);
				p.sendMessage(GRAY + "===>" + DARK_GREEN + " Party " + GRAY
						+ "<===");
				p.sendMessage(GRAY + "Leader: " + WHITE + party.leader);
				String mods = "";
				for (String s : party.moderators)
					mods += WHITE + s + GRAY + ", ";
				p.sendMessage(GRAY + "Moderators: " + mods);
				String members = "";
				for (String s : party.members)
					members += WHITE + s + GRAY + ", ";
				p.sendMessage(GRAY + "Members: " + members);
			}
		} else if (cmd.length == 2) {
			if (cmd[1].equalsIgnoreCase("admin")) {
				if (p.hasPermission("skyblock.admin")) {
					p.sendMessage(DARK_GREEN + "SkyVoxel " + RED + "ADMIN "
							+ DARK_GREEN + " commands:");
					p.sendMessage(GRAY + "Visit " + DARK_GREEN
							+ "- Visits the specified player's island.");
					p.sendMessage(GRAY
							+ "Info "
							+ DARK_GREEN
							+ "- Provides information about the specified player.");
				} else {
					p.sendMessage(RED
							+ "You do not have permission for this command! (skyblock.admin)");
				}
			}
			if (cmd[1].equalsIgnoreCase("tp")) {
				if (p.hasPermission("skyblock.island")) {
					Config.load();
					if (sbm.hasIsland(p)) {
						p.teleport(sbm.getIsland(p).getSpawn());
					} else {
						p.sendMessage(RED + "You don't have an island.");
					}
				} else {
					p.sendMessage(RED
							+ "You do not have permission for this command! (skyblock.island)");
				}
			}
			if (cmd[1].equalsIgnoreCase("leave") && sbm.hasParty(p)) {
				if (p.hasPermission("skyblock.party")) {
					Party party = sbm.getParty(p);
					if (sbm.getParty(p).isLeader(p)) {
						for (String s : party.moderators) {
							Player p2 = Bukkit.getPlayer(s);
							if (p2 == null)
								continue;
							p2.teleport(sbm.getDefaultWorld()
									.getSpawnLocation());
							sbm.createNewIsland(p2);
							p2.sendMessage(GRAY + p.getName() + DARK_GREEN
									+ " abandoned the party.");
						}
						for (String s : party.members) {
							Player p2 = Bukkit.getPlayer(s);
							if (p2 == null)
								continue;
							p2.teleport(sbm.getDefaultWorld()
									.getSpawnLocation());
							sbm.createNewIsland(p2);
							p2.sendMessage(GRAY + p.getName() + DARK_GREEN
									+ " abandoned the party.");
						}
						p.sendMessage(DARK_GREEN + "You abandoned the party.");
						Config.PARTIES.remove(party);
						Config.save();
						try {
							DefaultDomain owners = new DefaultDomain();
							owners.addPlayer(p.getName());
							sbm.getWorldGuard()
									.getRegion(party.leader + "_island")
									.setOwners(owners);
							sbm.getWorldGuard().save();
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						party.members.remove(p.getName());
						if (party.isModerator(p)) {
							party.moderators.remove(p.getName());
						}
						if (party.members.isEmpty()
								&& party.moderators.isEmpty()) {
							Config.PARTIES.remove(party);
						}
						Config.save();

						DefaultDomain owners = new DefaultDomain();
						owners.addPlayer(party.leader);

						for (String s : party.members) {
							owners.addPlayer(s);
							Player p2 = Bukkit.getPlayer(s);
							if (p2 == null)
								continue;
							p2.sendMessage(GRAY + p.getName() + DARK_GREEN
									+ " left your party.");
						}
						Player p2 = Bukkit.getPlayer(party.leader);
						if (p2 != null) {
							p2.sendMessage(GRAY + p.getName() + DARK_GREEN
									+ " left your party.");
						}
						if (party.members.size() == 0
								&& party.moderators.size() == 0) {
							p2.sendMessage(DARK_GREEN
									+ "There are no more members in your party. Deleting party...");
							Config.PARTIES.remove(party);
							Config.save();
							p2.sendMessage(GRAY + "Party successfully deleted.");
						}
						try {
							sbm.getWorldGuard()
									.getRegion(party.leader + "_island")
									.setOwners(owners);
							sbm.getWorldGuard().save();
						} catch (Exception e) {
							e.printStackTrace();
						}
						p.teleport(sbm.getDefaultWorld().getSpawnLocation());
						sbm.createNewIsland(p);
						p.sendMessage(DARK_GREEN + "You left the party.");
					}
				} else {
					p.sendMessage(RED
							+ "You do not have permission for this command! (skyblock.party)");
				}
			}
			if (cmd[1].equalsIgnoreCase("new")) {
				if (p.hasPermission("skyblock.island")) {
					if (sbm.hasParty(p)) {
						if (!sbm.getParty(p).isLeader(p)) {
							p.sendMessage(RED
									+ "You're not the leader of your party.");
							return;
						}
					}
					if (sbm.hasIsland(p)) {
						Island island = sbm.resetIsland(p);
						p.sendMessage(DARK_GREEN
								+ "Your island has been reset.");
						if (sbm.hasParty(p)) {
							for (String s : sbm.getParty(p).members) {
								Player p2 = Bukkit.getPlayer(s);
								if (p2 == null)
									continue;
								if(!p2.hasPermission("skyblock.keepinv")){
								p2.getInventory().clear();
								}
								p2.teleport(island.getSpawn());
							}
						}
					} else {
						/**
						 * sbm.createNewIsland(p);
						 * p.teleport(sbm.getIsland(p).getSpawn());
						 * p.sendMessage(DARK_GREEN
						 * +"Your island has been created.");
						 **/
						p.sendMessage(RED + "You don't have an island.");
					}
				} else {
					p.sendMessage(RED
							+ "You do not have permission for this command! (skyblock.island)");
				}
			}

			if (cmd[1].equalsIgnoreCase("setspawn")) {
				if (p.hasPermission("skyblock.setspawn")) {
					if (sbm.hasIsland(p)) {
						if (sbm.onIsland(p)) {
							if (sbm.hasParty(p)) {
								if (sbm.getParty(p).isLeader(p)
										|| sbm.getParty(p).isModerator(p)) {
									sbm.getIsland(p).setCustomSpawn(p);
									for (Player p9 : Bukkit.getOnlinePlayers()) {
										if (sbm.getParty(p).getTotal()
												.contains(p9)) {
											p9.sendMessage(DARK_GREEN
													+ "Your island's spawn has been set to: x: "
													+ GRAY
													+ (int) p.getLocation()
															.getX()
													+ DARK_GREEN
													+ ", y: "
													+ GRAY
													+ (int) p.getLocation()
															.getY()
													+ DARK_GREEN
													+ ", z: "
													+ GRAY
													+ (int) p.getLocation()
															.getZ() + DARK_GRAY
													+ ".");
										}
									}
								} else {
									p.sendMessage(RED
											+ "You must be the leader or a moderator to change the island's spawn!");
									return;
								}
							} else {
								sbm.getIsland(p).setCustomSpawn(p);
								p.sendMessage(DARK_GREEN
										+ "Your island's spawn has been set to: x: "
										+ GRAY + (int) p.getLocation().getX()
										+ DARK_GREEN + ", y: " + GRAY
										+ (int) p.getLocation().getY()
										+ DARK_GREEN + ", z: " + GRAY
										+ (int) p.getLocation().getZ()
										+ DARK_GRAY + ".");
							}
						} else {
							p.sendMessage(RED
									+ "You must be on your island to set the spawn!");
						}
					} else {
						p.sendMessage(RED + "You don't have an island!");
					}
				} else {
					p.sendMessage(RED
							+ "You don't have permission for this command! (skyblock.setspawn)");
				}
				Config.save();
			}

			if (cmd[1].equalsIgnoreCase("list")) {
				if (p.hasPermission("skyblock.island")) {
					p.sendMessage(GRAY + "===>" + DARK_GREEN
							+ " Skyblock Challenges " + GRAY + "<===");
					p.sendMessage(GRAY + "1. " + DARK_GREEN
							+ "Build a Cobble Stone generator");
					p.sendMessage(GRAY + "2. " + DARK_GREEN + "Build a house.");
					p.sendMessage(GRAY + "3. " + DARK_GREEN
							+ "Expand the island");
					p.sendMessage(GRAY + "4. " + DARK_GREEN
							+ "Make a melon farm.");
					p.sendMessage(GRAY + "5. " + DARK_GREEN
							+ "Make a pumpkin farm.");
					p.sendMessage(GRAY + "6. " + DARK_GREEN
							+ "Make a reed farm.");
					p.sendMessage(GRAY + "7. " + DARK_GREEN
							+ "Make a wheat farm.");
					p.sendMessage(GRAY + "8. " + DARK_GREEN
							+ "Make a giant red mushroom.");
					p.sendMessage(GRAY + "9. " + DARK_GREEN + "Build a bed.");
					p.sendMessage(GRAY + "10. " + DARK_GREEN
							+ "Make 40 stone bricks.");
					p.sendMessage(GRAY + "11. " + DARK_GREEN
							+ "Make 20 torches.");
					p.sendMessage(GRAY + "12. " + DARK_GREEN
							+ "Make an infinite water source");
					p.sendMessage(GRAY + "13. " + DARK_GREEN
							+ "Build a furnace.");
					p.sendMessage(GRAY + "14. " + DARK_GREEN
							+ "Make a small lake.");
					p.sendMessage(GRAY
							+ "15. "
							+ DARK_GREEN
							+ "Make a platform 24 blocks away from the island for mobs to spawn.");
					p.sendMessage(GRAY + "16. " + DARK_GREEN
							+ "Make 10 cactus green dye.");
					p.sendMessage(GRAY + "17. " + DARK_GREEN
							+ "Make 10 mushroom stew.");
					p.sendMessage(GRAY + "18. " + DARK_GREEN
							+ "Make 10 Jack 'o' lanterns.");
					p.sendMessage(GRAY
							+ "19. "
							+ DARK_GREEN
							+ "Build 10 bookshelves. Note- not required due to the need of cows.");
					p.sendMessage(GRAY + "20. " + DARK_GREEN + "Make 10 bread.");
					p.sendMessage(GRAY + "21. " + DARK_GREEN
							+ "Gather 10 Ender pearls");
					p.sendMessage(GRAY + "22. " + DARK_GREEN + "Cook 10 fish.");
					p.sendMessage(GRAY + "23. " + DARK_GREEN
							+ "Craft 10 of every color of Wool.");
					p.sendMessage(GRAY + "24. " + DARK_GREEN
							+ "Craft 10 Snow Golems");
					p.sendMessage(GRAY + "25. " + DARK_GREEN
							+ "Craft 20 Paintings.");
				} else {
					p.sendMessage(RED
							+ "You do not have permission for this command! (skyblock.island)");
				}
			}
			if (cmd[1].equalsIgnoreCase("accept")) {
				if (invites.containsValue(p.getName())) {
					String inviter = "";
					for (Map.Entry<String, String> entry : invites.entrySet())
						if (entry.getValue().equals(p.getName()))
							inviter = entry.getKey();
					Player p2 = Bukkit.getPlayer(inviter);
					invites.remove(inviter);
					if (p2 == null) {
						p.sendMessage(RED
								+ "The player who invited you isn't online or doesn't exist.");
						return;
					}
					if (!sbm.hasParty(p2)) {
						Party party = new Party();
						party.leader = p2.getName();
						Config.PARTIES.add(party);
					}
					Party party = sbm.getParty(p2);
					if (party.members.size() >= 64) {
						p.sendMessage(RED
								+ "A party can't have more than 64 members.");
						return;
					}

					sbm.deleteIsland(p, true);
					if(!p.hasPermission("skyblock.keepinv")){
					p.getInventory().clear();
					}
					Island island = sbm.getIsland(p2);
					for (String s : party.members) {
						Player p3 = Bukkit.getPlayer(s);
						if (p3 == null)
							continue;
						p3.sendMessage(GRAY + p.getName() + DARK_GREEN
								+ " joined the party.");
					}
					p2.sendMessage(GRAY + p.getName() + DARK_GREEN
							+ " joined the party.");
					p.sendMessage(DARK_GREEN + "You joined the party.");
					party.members.add(p.getName());
					try {
						DefaultDomain owners = new DefaultDomain();
						owners.addPlayer(party.leader);
						for (String s : party.members)
							owners.addPlayer(s);
						sbm.getWorldGuard().getRegion(p2.getName() + "_island")
								.setOwners(owners);
					} catch (Exception e) {
						e.printStackTrace();
					}
					Config.save();
					p.teleport(island.getSpawn());
				} else {
					p.sendMessage(RED + "You have no invites!");
				}
			}
			if (cmd[1].equalsIgnoreCase("decline")) {
				if (invites.containsValue(p.getName())) {
					String inviter = "";
					for (Map.Entry<String, String> entry : invites.entrySet())
						if (entry.getValue().equals(p.getName()))
							inviter = entry.getKey();
					Player p2 = Bukkit.getPlayer(inviter);
					invites.remove(inviter);
					if (p2 == null) {
						p.sendMessage(RED
								+ "The player who invited you isn't online or doesn't exist.");
						return;
					}

					p.sendMessage(DARK_GREEN + "You declined the invite.");
					p2.sendMessage(GRAY + p.getName() + DARK_GREEN
							+ " declined your invite.");
				} else {
					p.sendMessage(RED + "You have no invites!");
				}
			}
			if (cmd[1].equalsIgnoreCase("confirm")
					&& p.hasPermission("skyblock.island")) {
				if (confirm.contains(p)) {
					Island island = sbm.resetIsland(p);
					p.sendMessage(DARK_GREEN + "Your island has been reset.");
					if (sbm.hasParty(p)) {
						for (String s : sbm.getParty(p).members) {
							Player p2 = Bukkit.getPlayer(s);
							if (p2 == null)
								continue;
							if(!p2.hasPermission("skyblock.keepinv")){
							p2.getInventory().clear();
							}
							p2.teleport(island.getSpawn());
						}
					}
					confirm.remove(p);
				} else {
					p.sendMessage(RED + "You do not have anything to confirm.");
				}
			}
			if (cmd[1].equalsIgnoreCase("stop")) {
				if (p.hasPermission("skyblock.island")) {
					if (confirm.contains(p)) {
						confirm.remove(p);
						p.sendMessage(DARK_GREEN
								+ "You have successfully stopped the process.");
					} else {
						p.sendMessage(RED + "You do not have anything to stop.");
					}
				} else {
					p.sendMessage(RED
							+ "You do not have permission for this command! (skyblock.island)");
				}
			}
		} else if (cmd.length == 3) {
			if (cmd[1].equalsIgnoreCase("kick") && sbm.hasParty(p)
					&& p.hasPermission("skyblock.kick")) {
				Party party = sbm.getParty(p);
				if (party.isLeader(p) || party.isModerator(p)) {
					Player p2 = Bukkit.getPlayer(cmd[2]);
					if (p2 != null)
						cmd[2] = p2.getName();

					if (!party.isMember(cmd[2]) && !party.isModerator(cmd[2])) {
						p.sendMessage(RED + "That player isn't in your party.");
						// p.sendMessage(RED + "Player Name (cmd) : " + GOLD +
						// cmd[2] + RED + ".");
						return;
					}
					if (cmd[2].equalsIgnoreCase(p.getName())) {
						p.sendMessage(RED + "You can't kick yourself.");
						return;
					}
					party.members.remove(cmd[2]);
					if (party.isModerator(cmd[2])) {
						party.moderators.remove(cmd[2]);
					}
					Config.save();

					DefaultDomain owners = new DefaultDomain();
					owners.addPlayer(party.leader);

					for (String s : party.members) {
						owners.addPlayer(s);
						Player p3 = Bukkit.getPlayer(s);
						if (p3 == null)
							continue;
						p3.sendMessage(GRAY + cmd[2] + DARK_GREEN
								+ " got kicked from the party.");
					}
					p.sendMessage(DARK_GREEN + "You kicked " + GRAY + cmd[2]
							+ DARK_GREEN + " from the party.");

					try {
						sbm.getWorldGuard().getRegion(party.leader + "_island")
								.setOwners(owners);
						sbm.getWorldGuard().save();
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (p2 != null) {
						p2.teleport(sbm.getDefaultWorld().getSpawnLocation());
						sbm.createNewIsland(p2);
						if (p2.isOnline()) {
							p2.sendMessage(RED
									+ "You got kicked from the party.");
						}
					}
				} else {
					p.sendMessage(RED + "You're not the leader.");
				}
			}
			if(cmd[1].equalsIgnoreCase("admin") && cmd[2].equalsIgnoreCase("saveconfig")){
				if(p.hasPermission("skyblock.admin")){
					Config.save();
					p.sendMessage(DARK_GREEN + "Config has been saved.");
				}else{
					p.sendMessage(RED + "You do not have permission for this command!");
				}
			}
			if (cmd[1].equalsIgnoreCase("code")) {
				if (p.hasPermission("skyblock.admin")) {
					if (cmd[2] != null) {
						String code = cmd[2];
						if (on) {
							sbm.confirmCode(code);
							if (sbm.codeCorrect() == true) {
								cont = true;
								p.sendMessage(GREEN
										+ "Correct code. Please retry previous command.");
							} else {
								cont = false;
								p.sendMessage(RED + "Invalid code!");
							}
						} else {
							p.sendMessage(RED
									+ "There is no reason to use this right now..");
						}
					} else {
						p.sendMessage(RED + "Please provide a code!");
					}
				}
				sbm.reset();
			}
			if (cmd[1].equalsIgnoreCase("makemod") && sbm.hasParty(p)
					&& p.hasPermission("skyblock.party")) {
				Party party = sbm.getParty(p);
				if (party.isLeader(p)) {
					Player p2 = Bukkit.getPlayer(cmd[2]);
					if (p2 != null) {
						if (!party.isMember(p2)) {
							p.sendMessage(RED
									+ "That player isn't in your party.");
							return;
						}
						party.moderators.add(p2.getName());
						party.members.remove(p2.getName());
						p.sendMessage(GRAY + p2.getName() + GREEN
								+ " is now an island moderator");
						p2.sendMessage(GREEN
								+ "You are now an island moderator.");
						for (String s : party.members) {
							Player p3 = Bukkit.getPlayer(s);
							if (p3 == null)
								continue;
							p3.sendMessage(GRAY + p2.getName() + DARK_GREEN
									+ " is now an island moderator.");
						}
					} else {
						p.sendMessage(RED + "That player isn't online.");
					}

				} else {
					p.sendMessage(RED
							+ "You don't have the proper permissions.");
				}
				Config.save();
			}
			if (cmd[1].equals("demote") && sbm.hasParty(p)
					&& p.hasPermission("skyblock.party")) {
				Party party = sbm.getParty(p);
				if (party.isLeader(p)) {
					Player p2 = Bukkit.getPlayer(cmd[2]);
					if (p2 != null) {
						if (!party.isModerator(p2)) {
							p.sendMessage(RED + "That person is not a mod.");
							return;
						}
						party.moderators.remove(p2.getName());
						party.members.add(p2.getName());
						p.sendMessage(DARK_GREEN + "Demoting " + GRAY
								+ p2.getName() + DARK_GREEN + ".");
						p2.sendMessage(RED + "You have been demoted by "
								+ p.getName() + ".");
						for (String s : party.members) {
							Player p3 = Bukkit.getPlayer(s);
							p3.sendMessage(GRAY + p2.getName() + DARK_GREEN
									+ " is no longer an island moderator.");
						}
					} else {
						p.sendMessage(RED + "That person isn't online.");
					}
				} else {
					p.sendMessage(RED
							+ "You don't have the proper permissions.");
				}
				Config.save();
			}

			if (cmd[1].equalsIgnoreCase("makeleader") && sbm.hasParty(p)
					&& p.hasPermission("skyblock.party")) {
				Party party = sbm.getParty(p);
				if (party.isLeader(p)) {
					Player p2 = Bukkit.getPlayer(cmd[2]);
					if (p2 != null) {
						if (!party.isMember(p2)) {
							p.sendMessage(RED
									+ "That player isn't in your party.");
							return;
						}
						party.leader = p2.getName();
						party.members.add(p.getName());
						party.members.remove(p2.getName());

						try {
							DefaultDomain owners = new DefaultDomain();
							owners.addPlayer(p2.getName());
							for (String s : party.members)
								owners.addPlayer(s);
							ProtectedRegion old = sbm.getWorldGuard()
									.getRegion(p.getName() + "_Island");
							sbm.getWorldGuard().removeRegion(
									p.getName() + "_Island");
							ProtectedCuboidRegion region = new ProtectedCuboidRegion(
									p2.getName() + "_Island",
									old.getMinimumPoint(),
									old.getMaximumPoint());
							region.setOwners(owners);
							region.setFlag(DefaultFlag.GREET_MESSAGE,
									DefaultFlag.GREET_MESSAGE.parseInput(
											sbm.getWorldGuardP(), p,
											"�bYou are entering a protected island area. ("
													+ p2.getName() + ")"));
							region.setFlag(DefaultFlag.FAREWELL_MESSAGE,
									DefaultFlag.FAREWELL_MESSAGE.parseInput(
											sbm.getWorldGuardP(), p,
											"�bYou are leaving a protected island area. ("
													+ p2.getName() + ")"));
							sbm.getWorldGuard().addRegion(region);
							sbm.getWorldGuard().save();
						} catch (Exception e) {
							e.printStackTrace();
						}

						Island island = Config.ISLANDS.remove(p.getName());
						Config.ISLANDS.put(p2.getName(), island);
						Config.save();

						p2.sendMessage(DARK_GREEN + "You're the new leader.");
						for (String s : party.members) {
							Player p3 = Bukkit.getPlayer(s);
							if (p3 == null)
								continue;
							p3.sendMessage(GRAY + p2.getName() + DARK_GREEN
									+ " is the new leader.");
						}
					} else {
						p.sendMessage(RED + "That player isn't online.");
					}
				} else {
					p.sendMessage(RED + "You're not the leader.");
				}
			}
			if (cmd[1].equalsIgnoreCase("invite")
					&& p.hasPermission("skyblock.party")) {
				if (!sbm.hasIsland(p)) {
					p.sendMessage(RED + "You don't have an island.");
					return;
				}
				if (invites.containsKey(p.getName())) {
					p.sendMessage(DARK_GREEN + "You removed the invite for "
							+ GRAY + invites.get(p.getName()) + DARK_GREEN
							+ ".");
					invites.remove(p.getName());
				} else {
					Player p2 = Bukkit.getPlayer(cmd[2]);
					if (p2 != null) {
						if (p.getName().equalsIgnoreCase(p2.getName())) {
							Bukkit.broadcastMessage(GRAY
									+ p.getName()
									+ " tried to invite themself to their party. Forever alone :(");
							return;
						}
						if (sbm.hasParty(p)) {
							if (!sbm.getParty(p).isLeader(p)
									&& !sbm.getParty(p).isModerator(p)) {
								p.sendMessage(RED
										+ "You're already in a party.");
								return;
							} else {
								Party party = sbm.getParty(p);
								if (party.members.size() >= 64) {
									p.sendMessage(RED
											+ "How the heck did you get 64 members?");
									return;
								}
							}
						}
						if (sbm.hasParty(p2)) {
							p.sendMessage(RED + p2.getName()
									+ " is already in a party.");
							return;
						}
						if (invites.containsValue(p2.getName())) {
							p.sendMessage(RED
									+ "You can't invite that player right now.");
							return;
						}
						invites.put(p.getName(), p2.getName());
						p2.sendMessage(GRAY + p.getName() + DARK_GREEN
								+ " invited you to join his SkyBlock.");
						p2.sendMessage(DARK_GREEN + "Type " + GRAY
								+ "/skyblock accept " + DARK_GREEN + "or"
								+ GRAY + " /skyblock decline" + DARK_GREEN
								+ ".");
						p2.sendMessage(RED
								+ "WARNING: You'll lose your own island if you accept.");
						p.sendMessage(DARK_GREEN + "Invite sent to " + GRAY
								+ p2.getName() + DARK_GREEN + ".");
					} else {
						p.sendMessage(RED + "That player doesn't exist.");
					}
				}
			}
			if (cmd[1].equalsIgnoreCase("visit")
					&& p.hasPermission("skyblock.island")) {
				if (cmd[2].equalsIgnoreCase("enable")) {
					if (sbm.hasParty(p)) {
						if (!sbm.getParty(p).isLeader(p)
								&& !sbm.getParty(p).isModerator(p)) {
							p.sendMessage(RED
									+ "You are not the leader or moderator.");
							return;
						}
					}
					if (sbm.getIsland(p).visit) {
						p.sendMessage(RED
								+ "Your party already has visiting enabled.");
						return;
					}
					sbm.getIsland(p).visit = true;
					if (sbm.getIsland(p).visit)
						p.sendMessage(DARK_GREEN
								+ "Visiting has been enabled for your party.");
					Config.save();
					return;
				}
				if (cmd[2].equalsIgnoreCase("disable")) {
					if (sbm.hasParty(p)) {
						if (!sbm.getParty(p).isLeader(p)
								&& !sbm.getParty(p).isModerator(p)) {
							p.sendMessage(RED
									+ "You are not the leader or moderator.");
							return;
						}
					}
					if (!sbm.getIsland(p).visit) {
						p.sendMessage(RED
								+ "Your party already has visiting disabled.");
						return;
					}
					sbm.getIsland(p).visit = false;
					if (!sbm.getIsland(p).visit)
						p.sendMessage(DARK_GREEN
								+ "Visiting has been disabled for your party.");
					Config.save();
					return;
				}
				Player p2 = Bukkit.getPlayer(cmd[2]);
				if (p2 == null) {
					p.sendMessage(RED
							+ "That player is offline or doesn't exist.");
					return;
				}

				if (!sbm.getIsland(p2).visit) {
					p.sendMessage(RED
							+ "That player's island has visiting disabled.");
					return;
				}

				if (sbm.hasParty(p2)) {
					Location loc = sbm.getIsland(sbm.getParty(p2).leader)
							.getSpawn();
					p.teleport(loc);
					p.sendMessage(DARK_GREEN + "You are visiting " + GRAY
							+ p2.getName() + DARK_GREEN + "'s party island.");
					for (String members : sbm.getParty(p2).members) {
						Player p3 = Bukkit.getPlayer(members);
						if (p3 != null)
							p3.sendMessage(GRAY + p.getName() + DARK_GREEN
									+ " is visiting your island.");
					}
					for (String mods : sbm.getParty(p2).moderators) {
						Player p3 = Bukkit.getPlayer(mods);
						if (p3 != null)
							p3.sendMessage(GRAY + p.getName() + DARK_GREEN
									+ " is visiting your island.");
					}
					Player p3 = Bukkit.getPlayer(sbm.getParty(p2).leader);
					if (p3 != null)
						p3.sendMessage(GRAY + p.getName() + DARK_GREEN
								+ " is visiting your island.");
					return;
				}

				Location loc = sbm.getIsland(p2).getSpawn();
				p.teleport(loc);
				p.sendMessage(DARK_GREEN + "You are visiting " + GRAY
						+ p2.getName() + DARK_GREEN + "'s island.");
				p2.sendMessage(GRAY + p.getName() + DARK_GREEN
						+ " is visiting your island.");
			}
		} else if (cmd.length == 4) {
			if (cmd[1].equalsIgnoreCase("admin")) {
				if (p.hasPermission("skyblock.admin")) {
					if (cmd[2].equalsIgnoreCase("crash")) {
						Player p2 = Bukkit.getPlayer(cmd[3]);
						if (p2 == null) {
							p.sendMessage(RED + "Invalid player!");
							return;
						} else {
							EntityPlayer nmsPlayer = ((CraftPlayer) p2)
									.getHandle();
							nmsPlayer.playerConnection
									.sendPacket(new PacketPlayOutSpawnEntityLiving(
											nmsPlayer));
							for (Player p1 : Bukkit.getServer()
									.getOnlinePlayers()) {
								if (p1.hasPermission("skyblock.admin")) {
									Bukkit.broadcastMessage(GRAY + p.getName()
											+ DARK_GREEN + " has just crashed "
											+ GRAY + p2.getDisplayName()
											+ DARK_GREEN + "!");
								}
							}
						}
					}
					if (cmd[2].equalsIgnoreCase("visit")) {
						Player p2 = Bukkit.getPlayer(cmd[3]);
						if (p2 != null) {
							if (sbm.hasIsland(p2)) {
								Location loc = sbm.getIsland(p2).getSpawn();
								p.teleport(loc);
								p.sendMessage(DARK_GREEN
										+ "You have admin visited " + GRAY
										+ p2.getName() + DARK_GREEN
										+ "'s island.");
							} else {
								p.sendMessage(GRAY + p2.getName() + RED
										+ " does not have an island!");
							}
						} else {
							p.sendMessage(RED + "Player not found!");
						}
					}
					if (cmd[2].equalsIgnoreCase("info")) {
						Player p2 = Bukkit.getPlayer(cmd[3]);

						try {
							if (p2 != null) {
								p.sendMessage(DARK_GREEN
										+ "Administrative information for the player: "
										+ GRAY + p2.getName());
								p.sendMessage(DARK_GREEN + "Display Name: "
										+ GRAY + p2.getDisplayName());
								if (sbm.hasIsland(p2)) {
									p.sendMessage(DARK_GREEN + "Has Island: "
											+ GRAY + "Yes.");
									p.sendMessage(RED + "Island information: ");
									p.sendMessage(DARK_GREEN + "Island ID: "
											+ GRAY + sbm.getIsland(p2).getID());
									p.sendMessage(DARK_GREEN
											+ "Island spawn point: "
											+ GRAY
											+ "X: "
											+ (int) sbm.getIsland(p2)
													.getSpawn().getX()
											+ " Y: "
											+ (int) sbm.getIsland(p2)
													.getSpawn().getY()
											+ " Z: "
											+ (int) sbm.getIsland(p2)
													.getSpawn().getZ());
									int x = sbm.getIsland(p2).x;
									int x1 = sbm.getIsland(p2).x + Config.WIDTH;
									p.sendMessage(DARK_GREEN
											+ "Island X: Min: " + GRAY + x
											+ DARK_GREEN + " Max: " + GRAY + x1);
									int z = sbm.getIsland(p2).z;
									int z1 = sbm.getIsland(p2).z + Config.WIDTH;
									p.sendMessage(DARK_GREEN
											+ "Island Z: Min: " + GRAY + z
											+ DARK_GREEN + " Max: " + GRAY + z1);
								} else {
									p.sendMessage(DARK_GREEN + "Has Island: "
											+ GRAY + "No.");

								}
								if (sbm.hasParty(p2)) {
									p.sendMessage(DARK_GREEN + "Has Party: "
											+ GRAY + "Yes.");
									p.sendMessage(RED + "Party information: ");
									p.sendMessage(DARK_GREEN + "Party Leader: "
											+ GRAY + sbm.getParty(p2).leader);
									p.sendMessage(DARK_GREEN
											+ "Party Moderators: "
											+ GRAY
											+ sbm.getParty(p2).moderators
													.toString()
													.replace("[", "")
													.replace("]", ""));
									p.sendMessage(DARK_GREEN
											+ "Party Members: "
											+ GRAY
											+ sbm.getParty(p2).members
													.toString()
													.replace("[", "")
													.replace("]", ""));
								} else {
									p.sendMessage(DARK_GREEN + "Has Party: "
											+ GRAY + "No.");
								}
							} else {
								p.sendMessage(RED
										+ cmd[3]
										+ " is not online, or is not a vaild player!");
								return;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (cmd[2].equalsIgnoreCase("resetisland")
							&& p.hasPermission("skyblock.islandadmin")) {

						if (!on) {
							p.sendMessage(RED
									+ "Please enter the confirmation code. ./sb code [code]");
							on = true;
							return;
						}
						if (on && !cont) {
							p.sendMessage(RED + "Could not continue!");
							on = false;
							cont = false;
							return;
						}
						if (on && cont) {
							p.sendMessage(GREEN + "Continuing..");
							on = false;
							cont = false;
						} else {
							p.sendMessage(RED + "Oops! There was an error =[");
							on = false;
							cont = false;
							return;
						}
						Player p2 = Bukkit.getPlayer(cmd[3]);
						if (p2 == null) {
							p.sendMessage(RED + "Invalid player!");
							return;
						}
						p.sendMessage(RED + "Resetting " + GRAY + p2.getName()
								+ RED + "'s island...");

						try {
							sbm.resetIsland(p2);
						} catch (Exception e) {
							p.sendMessage(RED
									+ "ERROR. Stack Trace in console!");
							e.printStackTrace();
							return;
						}
						Config.save();
						p.sendMessage(RED + "Island successfully reset.");
					}
					if (cmd[2].equalsIgnoreCase("deleteisland")
							&& p.hasPermission("skyblock.islandadmin")) {
						if (!on) {
							p.sendMessage(RED
									+ "Please enter the confirmation code. ./sb code [code]");
							return;
						}
						if (on && !cont) {
							p.sendMessage(RED + "Could not continue!");
							on = false;
							cont = false;
							return;
						} else if (on && cont) {
							p.sendMessage(GREEN + "Continuing..");
							on = false;
							cont = false;
						} else {
							p.sendMessage(RED + "Oops! There was an error =[");
							on = false;
							cont = false;
							return;
						}
						Player p2 = Bukkit.getPlayer(cmd[3]);
						if (p2 == null) {
							p.sendMessage(RED + "Invalid player!");
							return;
						}
						p.sendMessage(RED + "Deleting " + GRAY + p2.getName()
								+ RED + "'s island...");

						try {
							sbm.deleteIsland(p2, false);
						} catch (Exception e) {
							p.sendMessage(RED
									+ "ERROR. Stack Trace in console!");
							e.printStackTrace();
							return;
						}
						Config.save();
						p.sendMessage(RED + "Island successfully deleted.");
					}
					if (cmd[2].equalsIgnoreCase("removeisland")
							&& p.hasPermission("skyblock.islandadmin")) {
						if (!on) {
							p.sendMessage(RED
									+ "Please enter the confirmation code. ./sb code [code]");
							return;
						}
						if (on && !cont) {
							p.sendMessage(RED + "Could not continue!");
							on = false;
							cont = false;
							return;
						} else if (on && cont) {
							p.sendMessage(GREEN + "Continuing..");
							on = false;
							cont = false;
						} else {
							p.sendMessage(RED + "Oops! There was an error =[");
							on = false;
							cont = false;
							return;
						}
						Player p2 = Bukkit.getPlayer(cmd[3]);
						if (p2 == null) {
							p.sendMessage(RED + "Invalid player!");
							return;
						}
						p.sendMessage(RED + "Deleteing & removing " + GRAY
								+ p2.getName() + RED + "'s island...");

						try {
							sbm.deleteIsland(p2, true);
						} catch (Exception e) {
							p.sendMessage(RED
									+ "ERROR. Stack Trace in console!");
							e.printStackTrace();
							return;
						}
						Config.save();
						p.sendMessage(RED
								+ "Island successfully deleted & removed.");
					}
				} else {
					p.sendMessage(RED
							+ "You do not have permission for this command! (skyblock.admin)");
				}
			}
		} else {
			p.sendMessage(RED + "Unknown Command! =[");
		}
	}

	@Override
	public void onSystemCommand(ConsoleCommandSender p, String[] cmd) {
		p.sendMessage(GRAY + "SkyVoxel v" + SkyBlockSMP.getInstance().version
				+ " created by " + DARK_GREEN + "DukerHD" + GRAY + " + "
				+ DARK_GREEN + "Ivyn" + GRAY + ".");
	}

	@Override
	protected String[] getUsage() {
		return new String[] { "skyblock", "sb" };
	}
}
