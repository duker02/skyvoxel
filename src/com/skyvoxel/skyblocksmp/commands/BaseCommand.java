package com.skyvoxel.skyblocksmp.commands;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import com.skyvoxel.skyblocksmp.SkyBlockSMP;

/**
 * @author 'Ivyn
 */
public abstract class BaseCommand {
	public SkyBlockSMP plugin;
	
    public BaseCommand(SkyBlockSMP instance){
    	plugin = instance;
    }
    
    public abstract void onPlayerCommand(Player p, String[] cmd);
    public abstract void onSystemCommand(ConsoleCommandSender p, String[] cmd);
    protected abstract String[] getUsage();
}