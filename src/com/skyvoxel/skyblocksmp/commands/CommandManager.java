package com.skyvoxel.skyblocksmp.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
/**
 * @author 'Ivyn
 */
public class CommandManager {
    protected List<BaseCommand> commands;

    public CommandManager() {
        commands = new ArrayList<BaseCommand>();
    }

    public boolean dispatch(CommandSender sender, String label, String[] args) {
        BaseCommand match = null;
        
        for(BaseCommand cmd : commands){
        	for(String usage : cmd.getUsage()){
        		if(usage.equalsIgnoreCase(label)) match = cmd;
        	}
        }
        
        String tmp = label+" ";
        for(String s : args){
        	tmp += s+" ";
        }
        args = tmp.split(" ");

        if(match != null){
        	try{
        		if(sender instanceof Player){
        			match.onPlayerCommand((Player)sender, args);
        		} else
        		if(sender instanceof ConsoleCommandSender){
        			match.onSystemCommand((ConsoleCommandSender)sender, args);
        		}
                return true;
        	}catch(Exception e){
        		sender.sendMessage("�cError trying to execute the command.");
                e.printStackTrace();
        	}
        }

        return false;
    }

    public void registerCommand(BaseCommand command) {
        commands.add(command);
    }

    public void unRegisterCommand(BaseCommand command) {
        commands.remove(command);
    }
}