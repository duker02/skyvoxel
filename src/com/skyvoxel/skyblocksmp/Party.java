package com.skyvoxel.skyblocksmp;

import java.util.ArrayList;

import org.bukkit.entity.Player;

public class Party {
	public String leader = "";
	public ArrayList<String> members = new ArrayList<String>();
	public ArrayList<String> moderators = new ArrayList<String>();
	public ArrayList<String> total = new ArrayList<String>();
	
	public boolean isLeader(Player p){
		return leader.equalsIgnoreCase(p.getName());
	}
	
	public boolean isMember(Player p){
		return isMember(p.getName());
	}
	public boolean isMember(String str){
		return members.contains(str);
	}
	
	public boolean isModerator(Player p){
		return isModerator(p.getName());
	}
	public boolean isModerator(String s){
		return moderators.contains(s);
	}
	public ArrayList<String> getTotal(){
		total.add(leader);
		for(int i = 0; i < members.size(); i++){
			total.add(members.get(i));
		}
		for(int i = 0; i < moderators.size(); i++){
			total.add(moderators.get(i));
		}
		
		return total;
	}
}