package com.skyvoxel.skyblocksmp;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Location;



/** Config class **/
/** islands.yml + parties.yml **/
public class Config {
	public static int WIDTH, HEIGHT, LENGTH;
	public static HashMap<String, Island> ISLANDS;
	public static ArrayList<Island> EMPTY_ISLANDS;
	public static ArrayList<Party> PARTIES;
	public static int LAST_ISLAND;
	public static String loc;
	
	public static void load(){
		WIDTH = 120;
		HEIGHT = 64;
		LENGTH = 12;
		SkyBlockSMP plugin = SkyBlockSMP.getInstance();
		
		ISLANDS = new HashMap<String, Island>();
		EMPTY_ISLANDS = new ArrayList<Island>();
		LAST_ISLAND = 9000;
		try{
			File file = new File(plugin.getDataFolder(), "islands.yml");
			if(!file.exists()){
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			Configuration config = new Configuration(file);
			config.load();
			for(String key : config.getKeys("islands")){
				String tag = "islands."+key;
				Island island = new Island(0, 0);
				island.x = config.getInt(tag+".x", 0);
				island.z = config.getInt(tag+".z", 0);
				island.worldname = config.getString(tag+".spawn.world", "SkyBlock");
				island.x9 = config.getDouble(tag+".spawn.x", island.getDefaultSpawn().getX());
				island.y9 = config.getDouble(tag+".spawn.y", island.getDefaultSpawn().getY());
				island.z9 = config.getDouble(tag+".spawn.z", island.getDefaultSpawn().getZ());
				island.yaw = config.getFloat(tag+".spawn.yaw", island.getDefaultSpawn().getYaw());
				island.pitch = config.getFloat(tag+".spawn.pitch", island.getDefaultSpawn().getPitch());
				island.spawn = new Location(island.world, config.getDouble(tag+".spawn.x"), config.getDouble(tag+".spawn.y"), config.getDouble(tag+".spawn.z"), island.yaw, island.pitch);
				island.visit = config.getBoolean(tag+".visit");
				String state = config.getString(tag+".state", "empty");
				if(state.equalsIgnoreCase("claimed")){
					String owner = config.getString(tag+".owner", "Player");
					island.owner = owner;
					ISLANDS.put(owner, island);
				} else
				if(state.equalsIgnoreCase("empty")){
					EMPTY_ISLANDS.add(island);
				}
				
				if(config.getBoolean(tag+".last", false)){
					LAST_ISLAND = island.getID();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		if(LAST_ISLAND == 9000){
			Island island = new Island(0, 0);
			EMPTY_ISLANDS.add(island);
			LAST_ISLAND = island.getID();
		}
		
		PARTIES = new ArrayList<Party>();
		try{
			File file = new File(plugin.getDataFolder(), "parties.yml");
			if(!file.exists()){
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			Configuration config = new Configuration(file);
			config.load();
			for(String key : config.getKeys("parties")){
				String tag = "parties."+key;
				Party party = new Party();
				party.leader = key;
				party.members = (ArrayList<String>)config.getStringList(tag+".members");
				party.moderators = (ArrayList<String>)config.getStringList(tag+".moderators");
				PARTIES.add(party);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void save(){
		SkyBlockSMP plugin = SkyBlockSMP.getInstance();
		try{
			File file = new File(plugin.getDataFolder(), "islands.yml");
			if(!file.exists()){
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			Configuration config = new Configuration(file);
			for(Island island : ISLANDS.values()){
				String tag = "islands."+island.getID();
				config.setProperty(tag+".x", island.x);
				config.setProperty(tag+".z", island.z);
				config.setProperty(tag+".spawn.world", island.worldname);
				config.setProperty(tag+".spawn.x", island.customSpawn ? island.x9 : island.getDefaultSpawn().getX());
				config.setProperty(tag+".spawn.y", island.customSpawn ? island.y9 : island.getDefaultSpawn().getY());
				config.setProperty(tag+".spawn.z", island.customSpawn ? island.z9 : island.getDefaultSpawn().getZ());
				config.setProperty(tag+".spawn.yaw", island.customSpawn ? island.yaw : island.getDefaultSpawn().getYaw());
				config.setProperty(tag+".spawn.pitch", island.customSpawn ? island.pitch : island.getDefaultSpawn().getPitch());
				config.setProperty(tag+".owner", island.owner);
				config.setProperty(tag+".state", "claimed");
				config.setProperty(tag+".visit", island.visit);
				if(LAST_ISLAND == island.getID()){
					config.setProperty(tag+".last", true);
				}
			}
			for(Island island : EMPTY_ISLANDS){
				String tag = "islands."+island.getID();
				config.setProperty(tag+".x", island.x);
				config.setProperty(tag+".z", island.z);
				config.setProperty(tag+".state", "empty");
				if(LAST_ISLAND == island.getID()){
					config.setProperty(tag+".last", true);
				}
			}
			config.save();
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			File file = new File(plugin.getDataFolder(), "parties.yml");
			if(!file.exists()){
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			Configuration config = new Configuration(file);
			for(Party party : PARTIES){
				String tag = "parties."+party.leader;
				config.setProperty(tag+".members", party.members);
				config.setProperty(tag+".moderators", party.moderators);
			}
			config.save();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}