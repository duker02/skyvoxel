package com.skyvoxel.skyblocksmp;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class Configuration {
	private File fileFile;
	private YamlConfiguration ymlFile;
	
	public Configuration(String file){
		this(new File(file));
	}
	
	public Configuration(File file){
		fileFile = file;
		ymlFile = new YamlConfiguration();
	}
	
	public void load(){
		ymlFile = YamlConfiguration.loadConfiguration(fileFile);
	}
	
	public void save(){
		try{
			ymlFile.save(fileFile);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void setProperty(String s, Object obj){
		ymlFile.set(s, obj);
	}
	
	public boolean contains(String s) {
		return ymlFile.contains(s);
	}
	
	public Object get(String s) {
		return ymlFile.get(s);
	}
	
	public Object get(String s, Object obj) {
		return ymlFile.get(s, obj);
	}
	
	public boolean getBoolean(String s) {
		return ymlFile.getBoolean(s);
	}
	
	public boolean getBoolean(String s, boolean b) {
		return ymlFile.getBoolean(s, b);
	}
	
	public List<Boolean> getBooleanList(String s) {
		return ymlFile.getBooleanList(s);
	}
	
	public List<Byte> getByteList(String s) {
		return ymlFile.getByteList(s);
	}
	
	public List<Character> getCharacterList(String s) {
		return ymlFile.getCharacterList(s);
	}
	
	public double getDouble(String s) {
		return ymlFile.getDouble(s);
	}
	
	public double getDouble(String s, double d) {
		return ymlFile.getDouble(s, d);
	}
	
	public List<Double> getDoubleList(String s) {
		return ymlFile.getDoubleList(s);
	}
	
	public List<Float> getFloatList(String s) {
		return ymlFile.getFloatList(s);
	}
	
	public int getInt(String s) {
		return ymlFile.getInt(s);
	}

	public int getInt(String s, int i) {
		return ymlFile.getInt(s, i);
	}
	
	public int getFloat(String s) {
		return ymlFile.getInt(s);
	}

	public int getFloat(String s, float f) {
		return ymlFile.getInt(s, (int)f);
	}
	
	public List<Integer> getIntegerList(String s) {
		return ymlFile.getIntegerList(s);
	}
	
	public ItemStack getItemStack(String s) {
		return ymlFile.getItemStack(s);
	}
	
	public ItemStack getItemStack(String s, ItemStack stack) {
		return ymlFile.getItemStack(s, stack);
	}
	
	public List<?> getList(String s) {
		return ymlFile.getList(s);
	}
	
	public List<?> getList(String s, List<?> l) {
		return ymlFile.getList(s, l);
	}
	
	public long getLong(String s) {
		return ymlFile.getLong(s);
	}
	
	public long getLong(String s, long l) {
		return ymlFile.getLong(s, l);
	}
	
	public List<Long> getLongList(String s) {
		return ymlFile.getLongList(s);
	}
	
	public String getName() {
		return ymlFile.getName();
	}
	
	public OfflinePlayer getOfflinePlayer(String s) {
		return ymlFile.getOfflinePlayer(s);
	}
	
	public OfflinePlayer getOfflinePlayer(String s, OfflinePlayer player) {
		return ymlFile.getOfflinePlayer(s, player);
	}
	
	public List<Short> getShortList(String s) {
		return ymlFile.getShortList(s);
	}
	
	public String getString(String s) {
		return ymlFile.getString(s);
	}

	public String getString(String s1, String s2) {
		return ymlFile.getString(s1, s2);
	}

	public List<String> getStringList(String s) {
		return ymlFile.getStringList(s);
	}

	public Vector getVector(String s) {
		return ymlFile.getVector(s);
	}

	public Vector getVector(String s, Vector arg1) {
		return ymlFile.getVector(s);
	}
	
	public Set<String> getKeys(String s){
		try{
			return ymlFile.getConfigurationSection(s).getKeys(false);
		}catch(Exception e){
			return new HashSet<String>();
		}
	}
	
	public Map<String, Object> getAll(){
		return ymlFile.getDefaultSection().getValues(true);
	}
	
}