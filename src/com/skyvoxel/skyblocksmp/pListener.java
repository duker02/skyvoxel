package com.skyvoxel.skyblocksmp;

import static org.bukkit.ChatColor.DARK_GREEN;
import static org.bukkit.ChatColor.GRAY;
import static org.bukkit.ChatColor.GREEN;
import static org.bukkit.ChatColor.RED;

import java.util.ArrayList;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.skyvoxel.skyblocksmp.commands.SkyBlockCommand;

public class pListener implements Listener {
	
	ArrayList<Player> global = new ArrayList<Player>();
	ArrayList<Player> isl = new ArrayList<Player>();

	public pListener(SkyBlockSMP plugin) {
		PluginManager pm = plugin.getServer().getPluginManager();
		pm.registerEvents(this, plugin);
	}

	/** @EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerJoin(PlayerJoinEvent ev) {
		Player p = ev.getPlayer();
		SkyBlockManager sbm = SkyBlockManager.getInstance();

		if (!sbm.hasIsland(p)) {
			sbm.createNewIsland(p);
		}
	} **/

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerRespawn(PlayerRespawnEvent ev) {
		SkyBlockManager sbm = SkyBlockManager.getInstance();
		ev.setRespawnLocation(sbm.getDefaultWorld().getSpawnLocation());
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerDropItem(PlayerDropItemEvent ev) {
		Player p = ev.getPlayer();
		SkyBlockManager sbm = SkyBlockManager.getInstance();

		if (p.getWorld().equals(sbm.getDefaultWorld())) {
			ev.setCancelled(true);
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.NORMAL)
	public void onInventoryClick(InventoryClickEvent e) {
		SkyBlockManager sbm = SkyBlockManager.getInstance();
		Player p = (Player) e.getWhoClicked();
		if (e.getInventory().getName().equals(ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY + "|" + ChatColor.GREEN + " Commands") && e.getCurrentItem().getTypeId() != 0) {
			e.setCancelled(true);
			if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�d�lIsland Teleport")) {
				p.closeInventory();
				p.teleport(sbm.getIsland(p).getSpawn());
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�e�lIsland Reset")) {
				p.closeInventory();
				if(sbm.hasParty(p)){
					if(!sbm.getParty(p).isLeader(p)){
						p.sendMessage(RED+"You're not the leader of your party.");
						return;
					}
				}
				SkyBlockCommand.confirm.add(p);
				p.sendMessage(DARK_GREEN+"Are you sure you want to reset your island?");
				p.sendMessage(DARK_GREEN+"If you choose to reset, type "+GRAY+"/sb confirm");
				p.sendMessage(DARK_GREEN+"If you don't want to reset, type "+GRAY+"/sb stop");
				/**Island island = sbm.resetIsland(p);
				p.sendMessage(DARK_GREEN+"Your island has been reset.");
				if(sbm.hasParty(p)){
					for(String s : sbm.getParty(p).members){
						Player p2 = Bukkit.getPlayer(s); if(p2 == null) continue;
						p2.getInventory().clear();
						p2.teleport(island.getSpawn());
					}
				}**/
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�b�lSkyBlock Challenges List")) {
				p.closeInventory();
				p.sendMessage(GRAY+"===>"+DARK_GREEN+" Skyblock Challenges "+GRAY+"<===");
				p.sendMessage(GRAY+"1. "+DARK_GREEN+"Build a Cobble Stone generator");
				p.sendMessage(GRAY+"2. "+DARK_GREEN+"Build a house.");
				p.sendMessage(GRAY+"3. "+DARK_GREEN+"Expand the island");
				p.sendMessage(GRAY+"4. "+DARK_GREEN+"Make a melon farm.");
				p.sendMessage(GRAY+"5. "+DARK_GREEN+"Make a pumpkin farm.");
				p.sendMessage(GRAY+"6. "+DARK_GREEN+"Make a reed farm.");
				p.sendMessage(GRAY+"7. "+DARK_GREEN+"Make a wheat farm.");
				p.sendMessage(GRAY+"8. "+DARK_GREEN+"Make a giant red mushroom.");
				p.sendMessage(GRAY+"9. "+DARK_GREEN+"Build a bed.");
				p.sendMessage(GRAY+"10. "+DARK_GREEN+"Make 40 stone bricks.");
				p.sendMessage(GRAY+"11. "+DARK_GREEN+"Make 20 torches.");
				p.sendMessage(GRAY+"12. "+DARK_GREEN+"Make an infinite water source");
				p.sendMessage(GRAY+"13. "+DARK_GREEN+"Build a furnace.");
				p.sendMessage(GRAY+"14. "+DARK_GREEN+"Make a small lake.");
				p.sendMessage(GRAY+"15. "+DARK_GREEN+"Make a platform 24 blocks away from the island for mobs to spawn.");
				p.sendMessage(GRAY+"16. "+DARK_GREEN+"Make 10 cactus green dye.");
				p.sendMessage(GRAY+"17. "+DARK_GREEN+"Make 10 mushroom stew.");
				p.sendMessage(GRAY+"18. "+DARK_GREEN+"Make 10 Jack 'o' lanterns.");
				p.sendMessage(GRAY+"19. "+DARK_GREEN+"Build 10 bookshelves. Note- not required due to the need of cows.");
				p.sendMessage(GRAY+"20. "+DARK_GREEN+"Make 10 bread.");
				p.sendMessage(GRAY+"21. "+DARK_GREEN+"Gather 10 Ender pearls");
				p.sendMessage(GRAY+"22. "+DARK_GREEN+"Cook 10 fish.");
				p.sendMessage(GRAY+"23. "+DARK_GREEN+"Craft 10 of every color of Wool.");
				p.sendMessage(GRAY+"24. "+DARK_GREEN+"Craft 10 Snow Golems");
				p.sendMessage(GRAY+"25. "+DARK_GREEN+"Craft 20 Paintings.");
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�4�lParty Invite")) {
				p.closeInventory();
				sbm.openInviteGUI(p);
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�3�lVisit Island")){
				p.closeInventory();
				sbm.openVisitGUI(p);
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�4�lDisable �6�lVisit")) {
				p.closeInventory();
				sbm.getIsland(p).visit = false;
				ArrayList<String> players = new ArrayList<String>();
				players.addAll(sbm.getParty(p).members);
				players.addAll(sbm.getParty(p).moderators);
				players.add(sbm.getParty(p).leader);
				
				for(int i = 0; i < players.size(); i++){
					Player p1 = Bukkit.getPlayer(players.get(i));
					if(p1 != null){
						p1.sendMessage(ChatColor.DARK_GREEN + "Your island visitation has been disabled by " + GRAY + p.getDisplayName() + DARK_GREEN + "!");
					}
				}

			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�2�lEnable �6�lVisit")) {
				
				p.closeInventory();
				sbm.getIsland(p).visit = true;
				ArrayList<String> players = new ArrayList<String>();
				players.addAll(sbm.getParty(p).members);
				players.addAll(sbm.getParty(p).moderators);
				players.add(sbm.getParty(p).leader);
				
				for(int i = 0; i < players.size(); i++){
					Player p1 = Bukkit.getPlayer(players.get(i));
					if(p1 != null){
						p1.sendMessage(ChatColor.DARK_GREEN + "Your island visitation has been enabled by " + GRAY + p.getDisplayName() + DARK_GREEN + "!");
					}
				}
				
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�6�lInvite Accept")) {
				p.closeInventory();
				String inviter = ""; SkyBlockCommand.getInstance();
				for(Map.Entry<String, String> entry : SkyBlockCommand.invites.entrySet()) if(entry.getValue().equals(p.getName())) inviter = entry.getKey();
				Player p2 = Bukkit.getPlayer(inviter);
				SkyBlockCommand.getInstance();
				SkyBlockCommand.invites.remove(inviter);
				if(p2 == null){
					p.sendMessage(RED+"The player who invited you isn't online or doesn't exist.");
					return;
				}
				if(!sbm.hasParty(p2)){
					Party party = new Party();
					party.leader = p2.getName();
					Config.PARTIES.add(party);
				}
				Party party = sbm.getParty(p2);
				if(party.members.size() >= 64){
					p.sendMessage(RED+"A party can't have more than 64 members.");
					return;
				}
				
				sbm.deleteIsland(p, true);
				p.getInventory().clear();
				Island island = sbm.getIsland(p2);
				for(String s : party.members){
					Player p3 = Bukkit.getPlayer(s);
					if(p3 == null) continue;
					p3.sendMessage(GRAY+p.getName()+DARK_GREEN+" joined the party.");
				}
				p2.sendMessage(GRAY+p.getName()+DARK_GREEN+" joined the party.");
				p.sendMessage(DARK_GREEN+"You joined the party.");
				party.members.add(p.getName());
				try{
					DefaultDomain owners = new DefaultDomain();
					owners.addPlayer(party.leader);
					for(String s : party.members) owners.addPlayer(s);
					sbm.getWorldGuard().getRegion(p2.getName()+"_island").setOwners(owners);
				}catch(Exception ex){
					ex.printStackTrace();
				}
				Config.save();
				p.teleport(island.getSpawn());
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�c�lInvite Decline")) {
				p.closeInventory();
				String inviter = ""; SkyBlockCommand.getInstance();
				for(Map.Entry<String, String> entry : SkyBlockCommand.invites.entrySet()) if(entry.getValue().equals(p.getName())) inviter = entry.getKey();
				Player p2 = Bukkit.getPlayer(inviter);
				SkyBlockCommand.getInstance();
				SkyBlockCommand.invites.remove(inviter);
				if(p2 == null){
					p.sendMessage(RED+"The player who invited you isn't online or doesn't exist.");
					return;
				}
				
				p.sendMessage(DARK_GREEN+"You declined the invite.");
				p2.sendMessage(GRAY+p.getName()+DARK_GREEN+" declined your invite.");
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�a�lParty Leave")) {
				p.closeInventory();
				Party party = sbm.getParty(p);
				if(sbm.getParty(p).isLeader(p)){
					for (String s : party.moderators) {
						Player p2 = Bukkit.getPlayer(s);
						if(p2 == null) continue;
						p2.teleport(sbm.getDefaultWorld().getSpawnLocation());
						sbm.createNewIsland(p2);
						p2.sendMessage(GRAY+p.getName()+DARK_GREEN+" abandoned the party.");
					}
					for (String s : party.members) {
						Player p2 = Bukkit.getPlayer(s);
						if(p2 == null) continue;
						p2.teleport(sbm.getDefaultWorld().getSpawnLocation());
						sbm.createNewIsland(p2);
						p2.sendMessage(GRAY+p.getName()+DARK_GREEN+" abandoned the party.");
						p.sendMessage(DARK_GREEN+"You abandoned the party.");
						Config.PARTIES.remove(party);
						Config.save();
						try{
							DefaultDomain owners = new DefaultDomain();
							owners.addPlayer(p.getName());
							sbm.getWorldGuard().getRegion(party.leader+"_island").setOwners(owners);
							sbm.getWorldGuard().save();
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}
				} else {
					party.members.remove(p.getName());
					if(party.isModerator(p)){
						party.moderators.remove(p.getName());
					}
					if(party.members.isEmpty() && party.moderators.isEmpty()){
						Config.PARTIES.remove(party);
					}
					Config.save();
					
					DefaultDomain owners = new DefaultDomain();
					owners.addPlayer(party.leader);
					
					for(String s : party.members){
						owners.addPlayer(s);
						Player p2 = Bukkit.getPlayer(s);
						if(p2 == null) continue;
						p2.sendMessage(GRAY+p.getName()+DARK_GREEN+" left your party.");
					}
					Player p2 = Bukkit.getPlayer(party.leader);
					if(p2 != null){
						p2.sendMessage(GRAY+p.getName()+DARK_GREEN+" left your party.");
					}
					if(party.members.size() == 0 && party.moderators.size() == 0){
						p2.sendMessage(DARK_GREEN+"There are no more members in your party. Deleting party...");
						Config.PARTIES.remove(party);
						Config.save();
						p2.sendMessage(GRAY+"Party successfully deleted.");
					}
					try{
						sbm.getWorldGuard().getRegion(party.leader+"_island").setOwners(owners);
						sbm.getWorldGuard().save();
					}catch(Exception ex){
						ex.printStackTrace();
					}
					p.teleport(sbm.getDefaultWorld().getSpawnLocation());
					sbm.createNewIsland(p);
					p.sendMessage(DARK_GREEN+"You left the party.");
				}
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�e�lParty Kick")) {
				p.closeInventory();
				sbm.openKickGUI(p);
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�4�lParty Leader Change")) {
				p.closeInventory();
				sbm.openLeaderGUI(p);
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�6�lParty Moderator Promote")) {
				p.closeInventory();
				sbm.openPromoteGUI(p);
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("�c�lParty Moderator Demote")) {
				p.closeInventory();
				sbm.openDemoteGUI(p);
			}
		} else if (e.getInventory().getName().equals(ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY + "|" + ChatColor.GREEN + " Invite") && e.getCurrentItem().getTypeId() != 0) {
			e.setCancelled(true);
			p.closeInventory();
			if(!sbm.hasIsland(p)){
				p.sendMessage(RED+"You don't have an island.");
				return;
			}
			if(SkyBlockCommand.invites.containsKey(p.getName())){
				p.sendMessage(DARK_GREEN+"You removed the invite for "+GRAY+SkyBlockCommand.invites.get(p.getName())+DARK_GREEN+".");
				SkyBlockCommand.getInstance();
				SkyBlockCommand.invites.remove(p.getName());
			} else {
				Player p2 = Bukkit.getPlayer(e.getCurrentItem().getItemMeta().getDisplayName());
				if(p2 != null){
					if(p.getName().equalsIgnoreCase(p2.getName())){
						Bukkit.broadcastMessage(GRAY+p.getName()+" tried to invite themself to their party. Forever alone :(");
						return;
					}
					if(sbm.hasParty(p)){
						if(!sbm.getParty(p).isLeader(p) && !sbm.getParty(p).isModerator(p)){
							p.sendMessage(RED+"You're already in a party.");
							return;
						} else {
							Party party = sbm.getParty(p);
							if(party.members.size() >= 64){
								p.sendMessage(RED+"How the heck did you get 64 members?");
								return;
							}
						}
					}
					if(sbm.hasParty(p2)){
						p.sendMessage(RED+p2.getName()+" is already in a party.");
						return;
					}
					if(SkyBlockCommand.invites.containsValue(p2.getName())){
						p.sendMessage(RED+"You can't invite that player right now.");
						return;
					}
					SkyBlockCommand.invites.put(p.getName(), p2.getName());
					p2.sendMessage(GRAY+p.getName()+DARK_GREEN+" invited you to join his SkyBlock.");
					p2.sendMessage(DARK_GREEN+"Type "+GRAY+"/skyblock accept "+DARK_GREEN+"or"+GRAY+" /skyblock decline"+DARK_GREEN+".");
					p2.sendMessage(RED+"WARNING: You'll lose your own island if you accept.");
					p.sendMessage(DARK_GREEN+"Invite sent to "+GRAY+p2.getName()+DARK_GREEN+".");
				} else {
					p.sendMessage(RED+"That player doesn't exist.");
				}
			}
		} else if (e.getInventory().getName().equals(ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY + "|" + ChatColor.GREEN + " Visit") && e.getCurrentItem().getTypeId() != 0) {
			e.setCancelled(true);
			p.closeInventory();
			String name = e.getCurrentItem().getItemMeta().getDisplayName();
			/** Player p2 = Bukkit.getPlayer(name);
			if(p2 != null){
			Location loc = sbm.getIsland(sbm.getParty(p2).leader).getSpawn();
			p.teleport(loc);
			p.sendMessage(DARK_GREEN + "You have teleported to " + GRAY + p2.getDisplayName() + DARK_GREEN + "'s island.");
			}**/
			Player p2 = Bukkit.getPlayer(name);
			if (p2 == null) {
				p.sendMessage(RED+"That player is offline or doesn't exist.");
				return;
			}
			
			if (!sbm.getIsland(p2).visit) {
				p.sendMessage(RED+"That player's island has visiting disabled.");
				return;
			}
			
			if (sbm.hasParty(p2)) {
				Location loc = sbm.getIsland(sbm.getParty(p2).leader).getSpawn();
				p.teleport(loc);
				p.sendMessage(DARK_GREEN+"You are visiting "+GRAY+p2.getName()+DARK_GREEN+"'s party island.");
				for (String members : sbm.getParty(p2).members) {
					Player p3 = Bukkit.getPlayer(members);
					if (p3 != null) p3.sendMessage(GRAY+p.getName()+DARK_GREEN+" is visiting your island.");
				}
				for (String mods : sbm.getParty(p2).moderators) {
					Player p3 = Bukkit.getPlayer(mods);
					if (p3 != null) p3.sendMessage(GRAY+p.getName()+DARK_GREEN+" is visiting your island.");
				}
				Player p3 = Bukkit.getPlayer(sbm.getParty(p2).leader);
				if (p3 != null) p3.sendMessage(GRAY+p.getName()+DARK_GREEN+" is visiting your island.");
				return;
			}
			
			Location loc = sbm.getIsland(p2).getSpawn();
			p.teleport(loc);
			p.sendMessage(DARK_GREEN+"You are visiting "+GRAY+p2.getName()+DARK_GREEN+"'s island.");
			p2.sendMessage(GRAY+p.getName()+DARK_GREEN+" is visiting your island.");

		} else if (e.getInventory().getName().equals(ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY + "|" + ChatColor.GREEN + " Kick") && e.getCurrentItem().getTypeId() != 0) {
			e.setCancelled(true);
			p.closeInventory();
			Party party = sbm.getParty(p);
			String cmd = e.getCurrentItem().getItemMeta().getDisplayName();
			if(party.isLeader(p) || party.isModerator(p)){
				Player p2 = Bukkit.getPlayer(e.getCurrentItem().getItemMeta().getDisplayName());
				if(p2 != null) cmd = p2.getName();
				
				if(!party.isMember(cmd) && !party.isModerator(cmd)){
					p.sendMessage(RED+"That player isn't in your party.");
					return;
				}
				if(cmd.equalsIgnoreCase(p.getName())){
					p.sendMessage(RED+"You can't kick yourself.");
					return;
				}
				party.members.remove(cmd);
				if(party.isModerator(cmd)){
					party.moderators.remove(cmd);
				}
				Config.save();
				
				DefaultDomain owners = new DefaultDomain();
				owners.addPlayer(party.leader);
				
				for(String s : party.members){
					owners.addPlayer(s);
					Player p3 = Bukkit.getPlayer(s);
					if(p3 == null) continue;
					p3.sendMessage(GRAY+cmd+DARK_GREEN+" got kicked from the party.");
				}
				p.sendMessage(DARK_GREEN+"You kicked "+GRAY+cmd+DARK_GREEN+" from the party.");
				
				try{
					sbm.getWorldGuard().getRegion(party.leader+"_island").setOwners(owners);
					sbm.getWorldGuard().save();
				}catch(Exception ex){
					ex.printStackTrace();
				}
				if(p2 != null){
					p2.teleport(sbm.getDefaultWorld().getSpawnLocation());
					sbm.createNewIsland(p2);
					if(p2.isOnline()){
						p2.sendMessage(RED+"You got kicked from the party.");
					}
				}
			} else {
				p.sendMessage(RED+"You're not the leader.");
			}
		} else if (e.getInventory().getName().equals(ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY + "|" + ChatColor.GREEN + " Make Leader") && e.getCurrentItem().getTypeId() != 0) {
			e.setCancelled(true);
			p.closeInventory();
			Party party = sbm.getParty(p);
			if(party.isLeader(p)){
				Player p2 = Bukkit.getPlayer(e.getCurrentItem().getItemMeta().getDisplayName());
				if(p2 != null){
					if(!party.isMember(p2)){
						p.sendMessage(RED+"That player isn't in your party.");
						return;
					}
					party.leader = p2.getName();
					party.members.add(p.getName());
					party.members.remove(p2.getName());
					
					try{
						DefaultDomain owners = new DefaultDomain();
						owners.addPlayer(p2.getName());
						for(String s : party.members) owners.addPlayer(s);
						ProtectedRegion old = sbm.getWorldGuard().getRegion(p.getName()+"_Island");
						sbm.getWorldGuard().removeRegion(p.getName()+"_Island");
						ProtectedCuboidRegion region = new ProtectedCuboidRegion(p2.getName()+"_Island", old.getMinimumPoint(), old.getMaximumPoint());
						region.setOwners(owners);
						region.setFlag(DefaultFlag.GREET_MESSAGE, DefaultFlag.GREET_MESSAGE.parseInput(sbm.getWorldGuardP(), p, "�bYou are entering a protected island area. ("+p2.getName()+")"));
						region.setFlag(DefaultFlag.FAREWELL_MESSAGE, DefaultFlag.FAREWELL_MESSAGE.parseInput(sbm.getWorldGuardP(), p, "�bYou are leaving a protected island area. ("+p2.getName()+")"));
						sbm.getWorldGuard().addRegion(region);
						sbm.getWorldGuard().save();
					}catch(Exception ex){
						ex.printStackTrace();
					}
					
					Island island = Config.ISLANDS.remove(p.getName());
					Config.ISLANDS.put(p2.getName(), island);
					Config.save();
					
					p2.sendMessage(DARK_GREEN+"You're the new leader.");
					for(String s : party.members){
						Player p3 = Bukkit.getPlayer(s);
						if(p3 == null) continue;
						p3.sendMessage(GRAY+p2.getName()+DARK_GREEN+" is the new leader.");
					}
				} else {
					p.sendMessage(RED+"That player isn't online.");
				}
			} else {
				p.sendMessage(RED+"You're not the leader.");
			}
		} else if (e.getInventory().getName().equals(ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY + "|" + ChatColor.GREEN + " Promote") && e.getCurrentItem().getTypeId() != 0) {
			e.setCancelled(true);
			p.closeInventory();
			Party party = sbm.getParty(p);
			if(party.isLeader(p)){
				Player p2 = Bukkit.getPlayer(e.getCurrentItem().getItemMeta().getDisplayName());
				if(p2 != null){
					if(!party.isMember(p2)){
						p.sendMessage(RED+"That player isn't in your party.");
						return;
					}
					party.moderators.add(p2.getName());
					party.members.remove(p2.getName());
					p.sendMessage(GRAY+p2.getName()+GREEN+" is now an island moderator");
					p2.sendMessage(GREEN+"You are now an island moderator.");
					for(String s : party.members){
						Player p3 = Bukkit.getPlayer(s);
						if(p3 == null) continue;
						p3.sendMessage(GRAY+p2.getName()+DARK_GREEN+" is now an island moderator.");
					}
				} else {
					p.sendMessage(RED+"That player isn't online.");
				}
				
			}else{
				p.sendMessage(RED+"You don't have the proper permissions.");
			}
			Config.save();
		} else if (e.getInventory().getName().equals(ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY + "|" + ChatColor.GREEN + " Demote") && e.getCurrentItem().getTypeId() != 0) {
			e.setCancelled(true);
			p.closeInventory();
			Party party = sbm.getParty(p);
			if(party.isLeader(p)){
				Player p2 = Bukkit.getPlayer(e.getCurrentItem().getItemMeta().getDisplayName());
				if(p2 != null){
					if(!party.isModerator(p2)){
						p.sendMessage(RED+"That person is not a mod.");
						return;
					}
					party.moderators.remove(p2.getName());
					party.members.add(p2.getName());
					p.sendMessage(DARK_GREEN+"Demoting "+GRAY+p2.getName()+DARK_GREEN+".");
					p2.sendMessage(RED+"You have been demoted by "+p.getName()+".");
					for(String s : party.members){
						Player p3 = Bukkit.getPlayer(s);
						p3.sendMessage(GRAY+p2.getName()+DARK_GREEN+" is no longer an island moderator.");
					}
				} else{
					p.sendMessage(RED+"That person isn't online.");
				}
			} else{
				p.sendMessage(RED+"You don't have the proper permissions.");
			}
			Config.save();
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent e) {
		Player p = (Player) e.getPlayer();
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getTypeId() == 49) {
			if (p.getItemInHand().getType() == Material.BUCKET) {
				if (WorldGuardPlugin.inst().canBuild(p, e.getClickedBlock())) {
					p.getItemInHand().setTypeId(0);
					e.getClickedBlock().setTypeId(0);
					p.setItemInHand(new ItemStack(Material.LAVA_BUCKET, 1));
					if (p.getItemInHand().getType() == Material.LAVA_BUCKET) p.sendMessage("�2You have been given a lava bucket.");
				}
			}
		}
	}
	
	/**public Player randomLeader(Player p) {
		Party party = SkyBlockManager.getInstance().getParty(p);
		ArrayList<Player> randomMod = new ArrayList<Player>();
		ArrayList<Player> randMember = new ArrayList<Player>();
		for (String s : party.moderators) {
			Player p2 = Bukkit.getPlayer(s);
			if (p2 != null) randomMod.add(p2);
			if (!randomMod.isEmpty()) {
				Random r = new Random();
				int mod1 = r.nextInt(randomMod.size());
				Player mod = randomMod.get(mod1);
				return mod;
			}
			if (randomMod.isEmpty()) {
				for (String s2 : party.members) {
					Player p3 = Bukkit.getPlayer(s2);
					if (p3 != null) randMember.add(p3);
					if (!randMember.isEmpty()) {
						Random r = new Random();
						int mem1 = r.nextInt(randMember.size());
						Player mem = randMember.get(mem1);
						return mem;
					}
				}
			}
		}
		return null;
	}**/
	
	/**
	@SuppressWarnings("deprication")
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerChat(AsyncPlayerChatEvent e){
		SkyBlockManager sbm = SkyBlockManager.getInstance();
		if(isl.contains(e.getPlayer())){
			if(sbm.hasParty(e.getPlayer())){
				ArrayList<String> players = new ArrayList<String>();
				players.addAll(sbm.getParty(e.getPlayer()).members);
				players.addAll(sbm.getParty(e.getPlayer()).moderators);
				players.add(sbm.getParty(e.getPlayer()).leader);
				
				for(int i = 0; i < players.size(); i++){
					String pi = players.get(i);
					Player p = Bukkit.getPlayer(pi);
					if(p != null){
						e.setFormat(ChatColor.GOLD + e.getPlayer().getDisplayName() + ChatColor.BLUE + " > " + ChatColor.LIGHT_PURPLE + e.getMessage());
						p.sendMessage(e.getFormat());
					}
				}
				e.setCancelled(true);
			}
		}else{
			
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerPickupItem(PlayerPickupItemEvent ev) {
		Player p = ev.getPlayer();
		SkyBlockManager sbm = SkyBlockManager.getInstance();

		if(p.getWorld().equals(sbm.getDefaultWorld())){
			ev.setCancelled(true);
		}
	} **/
}
