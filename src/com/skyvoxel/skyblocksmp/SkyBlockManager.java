package com.skyvoxel.skyblocksmp;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;

import static org.bukkit.ChatColor.*;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.skyvoxel.skyblocksmp.commands.SkyBlockCommand;

public class SkyBlockManager {
	public String deleteIsland = "deleteIsland Function: ";
	public String resetIsland = "resetIsland Function: ";
	private static boolean right;
	private static SkyBlockManager instance;
	static{
		instance = new SkyBlockManager();
	}
	
	public static SkyBlockManager getInstance(){
		return instance;
	}
	
	public World getDefaultWorld(){
		return Bukkit.getWorlds().get(0);
	}
	
	public World getEmptyWorld(){
		return Bukkit.getWorld("SkyBlock");
	}
	
	public Island createNewIsland(Player p){
		Island next = getNext(p, true);
		generateSkyBlock(p, next.x, next.z);
		p.getInventory().clear();
		return next;
	}
	
	public RegionManager getWorldGuard(){
		WorldGuardPlugin wg = (WorldGuardPlugin)Bukkit.getPluginManager().getPlugin("WorldGuard");
		return wg.getRegionManager(getEmptyWorld());
	}
	
	public WorldGuardPlugin getWorldGuardP(){
		return (WorldGuardPlugin)Bukkit.getPluginManager().getPlugin("WorldGuard");
	}
	
	public Island resetIsland(Player p){
		System.out.println(GREEN + resetIsland + "Function call initialized. 1");
		Island island = getIsland(p);
		if(island == null) return null;
		System.out.println(YELLOW + resetIsland + "Island not null. 2");
		deleteIsland(p, false);
		generateSkyBlock(p, island.x, island.z);
		System.out.println(YELLOW + resetIsland + "Island generated. 3");
		p.getInventory().clear();
		System.out.println(YELLOW + resetIsland + "Player " + p.getDisplayName() + " inv cleared. 4");
		island.setDefaultSpawn();
		p.teleport(island.getSpawn());
		System.out.println(YELLOW + resetIsland + "Player " + p.getDisplayName() + "teleported to island. 5");
		System.out.println(GOLD + resetIsland + "Island successfully reset. 6 & FINAL");
		return island;

	}
	
	@SuppressWarnings("deprecation")
	public void deleteIsland(Player p, boolean remove){
		System.out.println(GREEN + deleteIsland + "Function call initialized.");
		Island island = null;
		if((island = getIsland(p)) == null) return;
		System.out.println(YELLOW + deleteIsland + "Island not null.");
		World world = getEmptyWorld();
		
		int x0 = island.x;
		int x1 = island.x+Config.WIDTH;
		System.out.println(YELLOW + deleteIsland + "x's: MIN " + Math.min(x0, x1) + "|| MAX: " + Math.max(x0, x1));
		int z0 = island.z;
		int z1 = island.z+Config.LENGTH;
		System.out.println(YELLOW + deleteIsland + "z's: MIN " + Math.min(z0, z1) + "|| MAX: " + Math.max(z0, z1));
		int y0 = 0;
		int y1 = world.getMaxHeight();
		
		for(int x = Math.min(x0, x1); x < Math.max(x0, x1); x++){
		  for(int z = Math.min(z0, z1); z < Math.max(z0, z1); z++){
			for(int y = y0; y < y1; y++){
				Block b = world.getBlockAt(x, y, z);
				if(b.getTypeId() != 0){
					if(b.getTypeId() == 54){
						Chest chest = (Chest)b.getState();
						chest.getInventory().clear();
						chest.update();
						String xcoord = Integer.toString(b.getX());
						String ycoord = Integer.toString(b.getY());
						String zcoord = Integer.toString(b.getZ());
						System.out.println(YELLOW + deleteIsland + "Chest inv cleared @ X:" + xcoord + ", Y:"+ ycoord + ", Z:"+ zcoord + ".");
					}
					b.setTypeId(0);
				}
			}
		  }
		}
		System.out.println(YELLOW + deleteIsland + "Island blocks deleted.");
		for(Entity entity : world.getEntities()){
			if(entity instanceof Player) continue;
			if(entity.getLocation().getX() >= Math.min(x0, x1) && entity.getLocation().getX() <= Math.max(x0, x1)
			&& entity.getLocation().getY() >= y0 && entity.getLocation().getY() <= y1
			&& entity.getLocation().getZ() >= Math.min(z0, z1) && entity.getLocation().getZ() <= Math.max(z0, z1)){
				entity.remove();
				System.out.println(YELLOW + deleteIsland + "Entity Player deleted.");
			}
		}
		
		System.out.println(YELLOW + deleteIsland + "Island deleted.");
		
		if(remove){
			System.out.println(RED + deleteIsland + "Remove = true. 1");
			p.getInventory().clear();
			System.out.println(RED + deleteIsland + "Cleared player inv. 2");
			island.owner = "";
			System.out.println(RED + deleteIsland + "Owner name set to blank. 3");
			Config.ISLANDS.remove(p.getName());
			System.out.println(RED + deleteIsland + "Removed island from config. 4");
			Config.EMPTY_ISLANDS.add(island);
			System.out.println(RED + deleteIsland + "Added island to config as EMPTY_ISAND. 5");
			Config.save();
			System.out.println(RED + deleteIsland + "Config Saved. 6");

			try{
				getWorldGuard().removeRegion(p.getName()+"_Island");
				getWorldGuard().save();
				System.out.println(RED + deleteIsland + "Successfully removed region.");
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println(RED + deleteIsland + "Island deleted and removed from config.");
		}
		System.out.println(GOLD + deleteIsland + "Island successfully deleted. FINAL");
	}
	public void confirmCode(String code){
		if(code.equalsIgnoreCase(getTime())){
			right = true;
		}else{
			right = false;
		}
	}
	private Island getNext(Player p, boolean add){
		Island next = null;
		if(Config.EMPTY_ISLANDS.size() > 0){
			next = Config.EMPTY_ISLANDS.remove(0);
		} else {
            next = nextIslandLocation(getLast());
            Config.LAST_ISLAND = next.getID();
            while(islandExists(next)){
            	next = nextIslandLocation(next);
            }
            Config.LAST_ISLAND = next.getID();
		}
		if(add){
			next.owner = p.getName();
			Config.ISLANDS.put(p.getName(), next);
			Config.save();
			
			BlockVector min = new BlockVector(Math.min(next.x, next.x+Config.WIDTH), 0, Math.min(next.z, next.z+Config.LENGTH));
			BlockVector max = new BlockVector(Math.max(next.x, next.x+Config.WIDTH), getEmptyWorld().getMaxHeight(), Math.max(next.z, next.z+Config.LENGTH));
			DefaultDomain owners = new DefaultDomain();
			owners.addPlayer(p.getName());
			
			try{
				ProtectedCuboidRegion region = new ProtectedCuboidRegion(p.getName()+"_Island", min, max);
				region.setOwners(owners);
				region.setFlag(DefaultFlag.GREET_MESSAGE, DefaultFlag.GREET_MESSAGE.parseInput(getWorldGuardP(), p, ChatColor.AQUA+"You are entering a protected island area. ("+p.getName()+")"));
				region.setFlag(DefaultFlag.FAREWELL_MESSAGE, DefaultFlag.FAREWELL_MESSAGE.parseInput(getWorldGuardP(), p, ChatColor.AQUA+"You are leaving a protected island area. ("+p.getName()+")"));
				region.setFlag(DefaultFlag.USE, DefaultFlag.USE.parseInput(getWorldGuardP(), p, "deny"));
				region.setFlag(DefaultFlag.MOB_DAMAGE, DefaultFlag.MOB_DAMAGE.parseInput(getWorldGuardP(), p, "deny"));
				region.setFlag(DefaultFlag.POTION_SPLASH, DefaultFlag.POTION_SPLASH.parseInput(getWorldGuardP(), p, "deny"));
				getWorldGuard().addRegion(region);
				getWorldGuard().save();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return next;
	}
	private String getTime(){
		return "confirmcode987";
	}
	private Island nextIslandLocation(Island last){
		int x = last.x;
		int z = last.z;
		Island nextPos = new Island(x, z);
		if(x < z){
			if((-1 * x) < z){
				nextPos.x = nextPos.x + Config.WIDTH;
				return nextPos;
			}
			nextPos.z = nextPos.z + Config.LENGTH;
			return nextPos;
		}
		if(x > z){
			if((-1 * x) >= z){
				nextPos.x = nextPos.x - Config.WIDTH;
				return nextPos;
			}
			nextPos.z = nextPos.z - Config.LENGTH;
			return nextPos;
		}
		if(x <= 0){
			nextPos.z = nextPos.z + Config.LENGTH;
			return nextPos;
		}
		nextPos.z = nextPos.z - Config.LENGTH;
		return nextPos;
	}
	
	public boolean hasIsland(Player p){
		return getIsland(p) != null;
	}
	
	public Island getIsland(Player p){
		if(hasParty(p) && !getParty(p).isLeader(p)){
			return getIsland(getParty(p).leader);
		} else
		if(Config.ISLANDS.containsKey(p.getName())){
			return Config.ISLANDS.get(p.getName());
		} else {
			return null;
		}
	}
	
	public Island getIsland(String p){
		if(Config.ISLANDS.containsKey(p)){
			return Config.ISLANDS.get(p);
		} else {
			return null;
		}
	}
	

	
	public boolean onIsland(Player p) {
		if (hasParty(p)) {
			if (getWorldGuard().getRegion(getParty(p).leader+"_Island").contains((int)p.getLocation().getX(), (int)p.getLocation().getY(), (int)p.getLocation().getZ())) {
				return true;
			}
			return false;
		} else {
			if (getWorldGuard().getRegion(p.getName()+"_Island").contains((int)p.getLocation().getX(), (int)p.getLocation().getY(), (int)p.getLocation().getZ())) {
				return true;
			}
			return false;
		}
	}
	

	
	private Island getLast(){
		for(Island island : Config.ISLANDS.values()){
			if(island.getID() == Config.LAST_ISLAND){
				return island;
			}
		}
		return null;
	}
	
	private boolean islandExists(Island island){
		for(Island is : Config.ISLANDS.values()){
			if(is.getID() == island.getID()){
				return true;
			}
		}
		return false;
	}
	
	
	@SuppressWarnings("deprecation")
	private Island generateSkyBlock(Player p, int startX, int startZ){
		Island island = new Island(startX, startZ);
		int centerX = island.getCenterX();
		int centerY = island.getCenterY();
		int centerZ = island.getCenterZ();
		World world = getEmptyWorld();

		for(int x = -1; x < 2; x++){
		  for(int y = 0; y < 3; y++){
			for(int z = -1; z < 5; z++){
				Block b = world.getBlockAt(centerX + x, centerY + y, centerZ + z);
				if(y == 2) b.setTypeId(2);
				else b.setTypeId(3);
			}
		  }
		}
		
		for(int x = 2; x < 5; x++){
		  for(int y = 0; y < 3; y++){
		    for(int z = -1; z < 2; z++){
		    	Block b = world.getBlockAt(centerX + x, centerY + y, centerZ + z);
				if(y == 2) b.setTypeId(2);
				else b.setTypeId(3);
		    }
		  }
		}
		
		world.generateTree(new Location(world, centerX + 4, centerY + 3, centerZ -1), TreeType.TREE);
		
		world.getBlockAt(centerX + 0, centerY + 0, centerZ + 0).setTypeId(7);
		world.getBlockAt(centerX + 0, centerY + 1, centerZ + 0).setTypeId(12);
		world.getBlockAt(centerX + 0, centerY + 1, centerZ + 3).setTypeId(12);
		world.getBlockAt(centerX + 3, centerY + 1, centerZ + 0).setTypeId(12);
		
		Block b = world.getBlockAt(centerX + 0, centerY + 3, centerZ + 4);
		b.setTypeId(54);
		Chest chest = (Chest)b.getState();
		Inventory inv = chest.getInventory();
        inv.clear();
        
        inv.addItem(
        	new ItemStack(Material.STRING, 12),
        	new ItemStack(Material.LAVA_BUCKET, 1),
        	new ItemStack(Material.BONE, 1),
        	new ItemStack(Material.SUGAR_CANE, 1),
        	new ItemStack(Material.RED_MUSHROOM, 1),
        	new ItemStack(Material.BROWN_MUSHROOM, 1),
        	new ItemStack(Material.ICE, 2),
        	new ItemStack(Material.PUMPKIN_SEEDS, 1),
        	new ItemStack(Material.MELON, 1),
        	new ItemStack(Material.CACTUS, 1)
        );
        if(p.hasPermission("skyblock.vip1")){
        	inv.addItem(
        		new ItemStack(Material.DIRT, 10),
        		new ItemStack(Material.SAND, 10)
        	);
        }
        if(p.hasPermission("skyblock.vip2")){
        	inv.addItem(
            	new ItemStack(Material.IRON_INGOT, 5)
            );
        }
        if(p.hasPermission("skyblock.vip3")){
        	inv.addItem(
                new ItemStack(Material.DIAMOND_PICKAXE, 1)
            );
        }
        chest.update(true);
        
        return island;
	}
	public boolean codeCorrect(){
		if(right == true){
			return true;
		}else{
			return false;
		}
	}
	public Party getParty(Player p){
		for(Party party : Config.PARTIES){
			if(party.isMember(p) || party.isLeader(p) || party.isModerator(p)) return party;
		}
		return null;
	}
	public void reset(){
		right = false;
	}
	public boolean hasParty(Player p){
		return getParty(p) != null;
	}
	
	public void openSkyGUI(Player p) {
		SkyBlockManager sbm = SkyBlockManager.getInstance();
		Inventory skyGUI = Bukkit.createInventory(null, 27, ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY + "|" + ChatColor.GREEN + " Commands");
		
		ItemStack tp = new ItemStack(Material.ENDER_PEARL, 1);
		ItemMeta tpmeta = (ItemMeta) tp.getItemMeta();
		tpmeta.setDisplayName("�d�lIsland Teleport");
		List<String> tplore = new ArrayList<String>();
		tplore.add("�eTeleport to your island.");
		tpmeta.setLore(tplore);
		tp.setItemMeta(tpmeta);
		skyGUI.addItem(tp);
		
		ItemStack ref = new ItemStack(Material.WORKBENCH, 1);
		ItemMeta refmeta = (ItemMeta) ref.getItemMeta();
		refmeta.setDisplayName("�e�lIsland Reset");
		List<String> reflore = new ArrayList<String>();
		reflore.add("�bRefresh your island, resetting all blocks and items.");
		refmeta.setLore(reflore);
		ref.setItemMeta(refmeta);
		skyGUI.addItem(ref);
		
		ItemStack vi = new ItemStack(Material.MAP, 1);
		ItemMeta vimeta = (ItemMeta) vi.getItemMeta();
		vimeta.setDisplayName("�3�lVisit Island");
		List<String> vilore = new ArrayList<String>();
		vilore.add("�6Teleport to a player's island.");
		vimeta.setLore(vilore);
		vi.setItemMeta(vimeta);
		skyGUI.addItem(vi);
		
		ItemStack list = new ItemStack(Material.PAPER, 1);
		ItemMeta listmeta = (ItemMeta) list.getItemMeta();
		listmeta.setDisplayName("�b�lSkyBlock Challenges List");
		List<String> listlore = new ArrayList<String>();
		listlore.add("�aView a list of SkyBlock challenges.");
		listmeta.setLore(listlore);
		list.setItemMeta(listmeta);
		skyGUI.addItem(list);
		
		ItemStack inv = new ItemStack(Material.NETHER_STAR, 1);
		ItemMeta invmeta = (ItemMeta) inv.getItemMeta();
		invmeta.setDisplayName("�4�lParty Invite");
		List<String> invlore = new ArrayList<String>();
		invlore.add("�6Invite players to your party.");
		invmeta.setLore(invlore);
		inv.setItemMeta(invmeta);
		skyGUI.addItem(inv);
		
		if(SkyBlockCommand.invites.containsValue(p.getName()) && p.hasPermission("skyblock.party")){
			ItemStack acc = new ItemStack(Material.REDSTONE_TORCH_ON, 1);
			ItemMeta accmeta = (ItemMeta) acc.getItemMeta();
			accmeta.setDisplayName("�6�lInvite Accept");
			List<String> acclore = new ArrayList<String>();
			acclore.add("�cAccept the party invite.");
			accmeta.setLore(acclore);
			acc.setItemMeta(accmeta);
			skyGUI.addItem(acc);
			
			ItemStack dec = new ItemStack(Material.TORCH, 1);
			ItemMeta decmeta = (ItemMeta) dec.getItemMeta();
			decmeta.setDisplayName("�c�lInvite Decline");
			List<String> declore = new ArrayList<String>();
			declore.add("�6Decline the party invite.");
			decmeta.setLore(declore);
			dec.setItemMeta(decmeta);
			skyGUI.addItem(dec);
		}
		
		if (sbm.hasParty(p) && p.hasPermission("skyblock.party")) {
			
			ItemStack leave = new ItemStack(Material.COMPASS, 1);
			ItemMeta leavemeta = (ItemMeta) leave.getItemMeta();
			leavemeta.setDisplayName("�a�lParty Leave");
			List<String> leavelore = new ArrayList<String>();
			leavelore.add("�bLeave your current party.");
			leavemeta.setLore(leavelore);
			leave.setItemMeta(leavemeta);
			skyGUI.addItem(leave);
			
			if (sbm.getParty(p).isModerator(p.getName())) {
				
				ItemStack kick = new ItemStack(Material.GOLD_BOOTS, 1);
				ItemMeta kickmeta = (ItemMeta) kick.getItemMeta();
				kickmeta.setDisplayName("�e�lParty Kick");
				List<String> kicklore = new ArrayList<String>();
				kicklore.add("�bKick players from your party.");
				kickmeta.setLore(kicklore);
				kick.setItemMeta(kickmeta);
				skyGUI.addItem(kick);
				
			}
			
			if (sbm.getParty(p).isLeader(p)) {
				
				ItemStack kick = new ItemStack(Material.GOLD_BOOTS, 1);
				ItemMeta kickmeta = (ItemMeta) kick.getItemMeta();
				kickmeta.setDisplayName("�e�lParty Kick");
				List<String> kicklore = new ArrayList<String>();
				kicklore.add("�bKick players from your party.");
				kickmeta.setLore(kicklore);
				kick.setItemMeta(kickmeta);
				skyGUI.addItem(kick);
				
				ItemStack leader = new ItemStack(Material.BEACON, 1);
				ItemMeta leadermeta = (ItemMeta) leader.getItemMeta();
				leadermeta.setDisplayName("�4�lParty Leader Change");
				List<String> leaderlore = new ArrayList<String>();
				leaderlore.add("�6Make someone else leader.");
				leadermeta.setLore(leaderlore);
				leader.setItemMeta(leadermeta);
				skyGUI.addItem(leader);
				
				ItemStack mod = new ItemStack(Material.MUSHROOM_SOUP, 1);
				ItemMeta modmeta = (ItemMeta) mod.getItemMeta();
				modmeta.setDisplayName("�6�lParty Moderator Promote");
				List<String> modlore = new ArrayList<String>();
				modlore.add("�cPromote a party member to moderator.");
				modmeta.setLore(modlore);
				mod.setItemMeta(modmeta);
				skyGUI.addItem(mod);
				
				ItemStack demote = new ItemStack(Material.BOWL, 1);
				ItemMeta demotemeta = (ItemMeta) demote.getItemMeta();
				demotemeta.setDisplayName("�c�lParty Moderator Demote");
				List<String> demotelore = new ArrayList<String>();
				demotelore.add("�6Demote a moderator to a regular party member.");
				demotemeta.setLore(demotelore);
				demote.setItemMeta(demotemeta);
				skyGUI.addItem(demote);
			}
		
		}
		
		if((sbm.hasParty(p) && sbm.getParty(p).isLeader(p) || sbm.getParty(p).isModerator(p)) || (!sbm.hasParty(p) && sbm.hasIsland(p))){
			
			ItemStack visittrue = new ItemStack(Material.WOOL, 1, (byte) 14);
			ItemStack visitfalse = new ItemStack(Material.WOOL, 1, (byte) 5);
			ItemMeta visitmetat = (ItemMeta) visittrue.getItemMeta();
			ItemMeta visitmetaf = (ItemMeta) visitfalse.getItemMeta();
			visitmetat.setDisplayName("�4�lDisable �6�lVisit");
			List<String> visittlore = new ArrayList<String>();
			visittlore.add("�cDeny players to visit your island.");
			visitmetaf.setDisplayName("�2�lEnable �6�lVisit");
			List<String> visitflore = new ArrayList<String>();
			visitflore.add("�eAllow players to visit your island.");
			visitmetat.setLore(visittlore);
			visitmetaf.setLore(visitflore);
			visittrue.setItemMeta(visitmetat);
			visitfalse.setItemMeta(visitmetaf);
			skyGUI.addItem(sbm.getIsland(p).visit == true ? visittrue : visitfalse);
		}
		
		p.openInventory(skyGUI);
	}

	public void openInviteGUI(Player p) {
		Inventory invitelist = Bukkit.createInventory(null, getInviteSize(),
				ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY
						+ "|" + ChatColor.GREEN + " Invite");
		for (Player online : Bukkit.getOnlinePlayers()) {
			ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
			SkullMeta meta = (SkullMeta) skull.getItemMeta();
			meta.setDisplayName(online.getName());
			skull.setItemMeta(meta);
			invitelist.addItem(skull);
		}
		p.openInventory(invitelist);
	}
	
	public void openVisitGUI(Player p) {
		Inventory visitlist = Bukkit.createInventory(null, getInviteSize(),
				ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY
						+ "|" + ChatColor.GREEN + " Visit");
		for (Player online : Bukkit.getOnlinePlayers()) {
			ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, getIsland(online).visit == true ? (byte) 3 : (byte) 1);
			SkullMeta meta = (SkullMeta) skull.getItemMeta();
			meta.setDisplayName(online.getName());
			skull.setItemMeta(meta);
			visitlist.addItem(skull);
		}
		p.openInventory(visitlist);
	}
	
	public void openKickGUI(Player p) {
		ArrayList<String> modArray = getParty(p).moderators;
		ArrayList<String> membersArray = getParty(p).members;
		Inventory invitelist = Bukkit.createInventory(null, getPartySize(p, true),
				ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY
						+ "|" + ChatColor.GREEN + " Kick");
		for (String online1 : membersArray) {
			ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1);
			skull.setDurability((short) 3);
			SkullMeta meta = (SkullMeta) skull.getItemMeta();
			meta.setDisplayName(online1);
			skull.setItemMeta(meta);
			invitelist.addItem(skull);
		}
		
		for (String online1 : modArray) {
			ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1);
			skull.setDurability((short) 3);
			SkullMeta meta = (SkullMeta) skull.getItemMeta();
			meta.setDisplayName(online1);
			skull.setItemMeta(meta);
			invitelist.addItem(skull);
		}
		
		
		p.openInventory(invitelist);
	}
	
	public void openPromoteGUI(Player p) {
		ArrayList<String> membersArray = getParty(p).members;
		Inventory invitelist = Bukkit.createInventory(null, getPartySize(p, false),
				ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY
						+ "|" + ChatColor.GREEN + " Promote");
		for (String online1 : membersArray) {
			ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1);
			skull.setDurability((short) 3);
			SkullMeta meta = (SkullMeta) skull.getItemMeta();
			meta.setDisplayName(online1);
			skull.setItemMeta(meta);
			invitelist.addItem(skull);
		}
		p.openInventory(invitelist);
	}
	
	public void openLeaderGUI(Player p) {
		ArrayList<String> modArray = getParty(p).moderators;
		ArrayList<String> membersArray = getParty(p).members;
		Inventory invitelist = Bukkit.createInventory(null, getPartySize(p, true),
				ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY
						+ "|" + ChatColor.GREEN + " Make Leader");
		for (String online1 : membersArray) {
			ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1);
			skull.setDurability((short) 3);
			SkullMeta meta = (SkullMeta) skull.getItemMeta();
			meta.setDisplayName(online1);
			skull.setItemMeta(meta);
			invitelist.addItem(skull);
		}
		
		for (String online1 : modArray) {
			ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1);
			skull.setDurability((short) 3);
			SkullMeta meta = (SkullMeta) skull.getItemMeta();
			meta.setDisplayName(online1);
			skull.setItemMeta(meta);
			invitelist.addItem(skull);
		}
		
		p.openInventory(invitelist);
	}
	
	public void openDemoteGUI(Player p) {
		ArrayList<String> modArray = getParty(p).moderators;
		Inventory invitelist = Bukkit.createInventory(null, getDemoteSize(p),
				ChatColor.DARK_GREEN + "SkyVoxel GUI " + ChatColor.DARK_GRAY
						+ "|" + ChatColor.GREEN + " Demote");
		for (String online1 : modArray) {
			ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1);
			skull.setDurability((short) 3);
			SkullMeta meta = (SkullMeta) skull.getItemMeta();
			meta.setDisplayName(online1);
			skull.setItemMeta(meta);
			invitelist.addItem(skull);

		}
		p.openInventory(invitelist);
	}

	
	
	
	public static int getInviteSize() {
		int size = Bukkit.getOnlinePlayers().length;
		while (!(size % 9 == 0)) {
			++size;
		}
		return (size > 54) ? 54 : size;
	}
	
	public static int getDemoteSize(Player p) {
		int size = SkyBlockManager.getInstance().getParty(p).moderators.size();
		while (!(size % 9 == 0)) {
			++size;
		}
		return (size > 54) ? 54 : size;
	}
	
	public static int getPartySize(Player p, boolean mods) {
		int size = SkyBlockManager.getInstance().getParty(p).members.size();
		if (mods) {
			size = SkyBlockManager.getInstance().getParty(p).members.size() + SkyBlockManager.getInstance().getParty(p).moderators.size();
		}
		while (!(size % 9 == 0)) {
			++size;
		}
		return (size > 54) ? 54 : size;
	}
}