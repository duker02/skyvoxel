package com.skyvoxel.skyblocksmp;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.Messenger;

import com.skyvoxel.skyblocksmp.commands.*;

public class SkyBlockSMP extends JavaPlugin{
	public String name, displayname, version, description, channel;
	private static Plugin instance;
	private CommandManager commandManager;
	public pListener pL;
		
    public void onDisable(){}

    public void onEnable() {
    	name = getDescription().getName();
    	displayname = getDescription().getFullName();
    	version = getDescription().getVersion();
        description = getDescription().getDescription();
        //setPacketChannel("name");
        
    	registerEvents();
    	registerCommands();
    	
    	instance = this;
    	if(Bukkit.getWorld("SkyBlock") == null){
    		Bukkit.createWorld(new WorldCreator("SkyBlock").environment(Environment.NORMAL).generator(getDefaultWorldGenerator("SkyBlock", "0")).generateStructures(false).type(WorldType.FLAT));
    	}
    	
    	Config.load();

    }
    
	public ChunkGenerator getDefaultWorldGenerator(String worldName, String id){
		return new ChunkGenerator(){
			public byte[][] generateBlockSections(World world, Random random, int chunkX, int chunkZ, BiomeGrid biomes) {
				byte[][] result = new byte[256 / 16][4096];
				return result;
			}
		};
	}
	
    public static SkyBlockSMP getInstance(){
    	return (SkyBlockSMP)instance;
    }
    public CommandManager getCommandManager() {
		return commandManager;
	}
    
    private void registerEvents(){
    	pL = new pListener(this);
    }
    
    private void registerCommands() {
        commandManager = new CommandManager();
        commandManager.registerCommand(new SkyBlockCommand(this));
    }
    
    public void sM(String message){
    	System.out.println("["+displayname+"] "+message);
    }
    
    public Player getPlayer(String name){
		for(Player pl : this.getServer().getOnlinePlayers()){
			if(pl.getName().toLowerCase().startsWith(name.toLowerCase())){
				return pl;
			}
		}
    	return null;
    }
    
    public void setPacketChannel(String channel){
    	Messenger m = Bukkit.getMessenger();
    	if(!m.isOutgoingChannelRegistered(this, channel)){
    		m.registerOutgoingPluginChannel(this, channel);
    	}
    	this.channel = channel;
    }

    public void sendGlobalPacket(String message){
    	sendGlobalPacket(message.getBytes());
    }
    public void sendGlobalPacket(byte[] message){
    	for(Player p : getServer().getOnlinePlayers()){
    		sendPlayerPacket(p, message);
    	}
    }
    
    public void sendPlayerPacket(Player p, String message){
    	sendPlayerPacket(p, message.getBytes());
    }
    public void sendPlayerPacket(Player p, byte[] message){
    	if(channel != null && channel.length() > 0){
    		p.sendPluginMessage(this, channel, message);
    	}
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return commandManager.dispatch(sender, label, args);
    }
}